//
//  RedCardFace.swift
//  EKS Flashcards
//
//  Created by Simon Budker on 3/8/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class RedCardFace: CardFace {

    var lastScalar = UInt32(0);

    func applyRed(_ hebrew :String, redIndexString :String?, targetMutableString :inout NSAttributedString) {

        if (redIndexString == nil){ return }
        let redHebrew = NSMutableAttributedString(string: hebrew)

        if (redHebrew.length != 0) {

            var indiciesToPaint = [Int]();
            if (redIndexString!.contains("-")) {
                let components = redIndexString!.components(separatedBy: "-")
                for substr in components {
                    indiciesToPaint.append(Int(String(substr))!)
                }
            } else {
                for char in redIndexString! {
                    indiciesToPaint.append(Int(String(char))!)
                }
            }


            var rangeStart = 0;
            var countedIndex = 0;
            var indexToPaint = 0;

            for scalar in redHebrew.string.unicodeScalars {

                if (isSkippedChar(scalar.value)) {
                    countedIndex -= 1;

                }

                if (indexToPaint < indiciesToPaint.count && countedIndex == indiciesToPaint[indexToPaint]){
                    let range = NSRange(location: rangeStart, length: 1)
                redHebrew.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: range);
                    indexToPaint += 1;
                }

                rangeStart += 1;
                countedIndex += 1;
                lastScalar = scalar.value
            }
            
            lastScalar = 0;

        }

        targetMutableString = redHebrew
    }




    fileprivate func isSkippedChar(_ value :UInt32) -> Bool {

        //uncount the holam vowel over the vav charachter
        if (lastScalar == 1493 && value == 1465){return true;}
        
        if (value == 1468 || value == 1473 || value == 1474){ return true } else { return false}
        
    }
}



