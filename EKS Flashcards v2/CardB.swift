//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

class CardB :Card {

    var leftHebrew :String;
    var leftEnglish :String;
    var leftItalics :String;

    var rightHebrew :String;
    var rightEnglish :String;
    var rightItalics :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, leftHebrew :String, leftEnglish :String, leftItalics :String, rightHebrew :String, rightEnglish :String, rightItalics :String) {

        self.leftEnglish = leftEnglish;
        self.leftHebrew = leftHebrew;
        self.leftItalics = leftItalics;

        self.rightEnglish = rightEnglish;
        self.rightHebrew = rightHebrew;
        self.rightItalics = rightItalics;

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);

    }

    required init(coder aDecoder: NSCoder) {


        self.leftHebrew = aDecoder.decodeObject(forKey: "leftHebrew") as! String;
        self.leftEnglish = aDecoder.decodeObject(forKey: "leftEnglish") as! String;
        self.leftItalics = aDecoder.decodeObject(forKey: "leftItalics") as! String;

        self.rightHebrew = aDecoder.decodeObject(forKey: "rightHebrew") as! String;
        self.rightEnglish = aDecoder.decodeObject(forKey: "rightEnglish") as! String;
        self.rightItalics = aDecoder.decodeObject(forKey: "rightItalics") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(leftHebrew, forKey: "leftHebrew");
        aCoder.encode(leftEnglish, forKey: "leftEnglish");
        aCoder.encode(leftItalics, forKey: "leftItalics");

        aCoder.encode(rightHebrew, forKey: "rightHebrew");
        aCoder.encode(rightEnglish, forKey: "rightEnglish");
        aCoder.encode(rightItalics, forKey: "rightItalics");

        super.encode(with: aCoder);
    }
}
