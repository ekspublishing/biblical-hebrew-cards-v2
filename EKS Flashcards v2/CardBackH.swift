//
//  CardBackE.swift
//  EKS Flashcards
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackH: CardFace {

    let nibName = "CardBackH"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var mainHebrew: UILabel!

    @IBOutlet var topHebrew: UILabel!
    @IBOutlet var middleHebrew1: UILabel!
    @IBOutlet var middleHebrew2: UILabel!
    @IBOutlet var bottomHebrew: UILabel!

    @IBOutlet var topEnglish: UILabel!
    @IBOutlet var middleEnglish1: UILabel!
    @IBOutlet var middleEnglish2: UILabel!
    @IBOutlet var bottomEnglish: UILabel!



    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        mainHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        topHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        middleHebrew1.font = UIFont(name: Catalog.fontName, size: 30);
        middleHebrew2.font = UIFont(name: Catalog.fontName, size: 30);
        bottomHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;

        mainHebrew.font = mainHebrew.font.withSize(fontSize * 2);

        topHebrew.font = topHebrew.font.withSize(fontSize);
        middleHebrew1.font = middleHebrew1.font.withSize(fontSize);
        middleHebrew2.font = middleHebrew2.font.withSize(fontSize);
        bottomHebrew.font = bottomHebrew.font.withSize(fontSize);

        topEnglish.font = topEnglish.font.withSize(fontSize);
        middleEnglish1.font = middleEnglish1.font.withSize(fontSize);
        middleEnglish2.font = middleEnglish1.font.withSize(fontSize);
        bottomEnglish.font = bottomEnglish.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        hebrewMaqafFormatting()

    }

    override func setCard(_ card :Card){

        if let card_H = card as? CardH {
            chapterDescriptionLabel.text = card_H.chapterDescription;

            self.mainHebrew.text = card_H.mainHebrew

            self.topHebrew.text = card_H.topHebrew
            self.middleHebrew1.text = card_H.middleHebrew1
            self.middleHebrew2.text = card_H.middleHebrew2
            self.bottomHebrew.text = card_H.bottomHebrew

            self.topEnglish.text = card_H.topEnglish
            self.middleEnglish1.text = card_H.middleEnglish1
            self.middleEnglish2.text = card_H.middleEnglish2
            self.bottomEnglish.text = card_H.bottomEnglish

            numericalIndexLabel.text = String(card_H.numericalIndex);
            backingCard = card;

            hebrewMaqafFormatting()
        }

    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let hCard = backingCard as! CardH

            let tHebrew = hCard.topHebrew as NSString
            let m1Hebrew = hCard.middleHebrew1 as NSString
            let m2Hebrew = hCard.middleHebrew2 as NSString
            let bHebrew = hCard.bottomHebrew as NSString

            let formattedTopHebrew = NSMutableAttributedString(string: tHebrew as String)
            let formattedM1Hebrew = NSMutableAttributedString(string: m1Hebrew as String)
            let formattedM2Hebrew = NSMutableAttributedString(string: m2Hebrew as String)
            let formattedBottomHebrew = NSMutableAttributedString(string: bHebrew as String)

            if (formattedTopHebrew.length != 0){
                let range = tHebrew.range(of: "־")
                formattedTopHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(topHebrew.font.pointSize/14), range: range)
            }
            topHebrew.attributedText = formattedTopHebrew

            if (formattedM1Hebrew.length != 0){
                let range = m1Hebrew.range(of: "־")
                formattedM1Hebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(middleHebrew1.font.pointSize/14), range: range)
            }
            middleHebrew1.attributedText = formattedM1Hebrew

            if (formattedM2Hebrew.length != 0){
                let range = m2Hebrew.range(of: "־")
                formattedM2Hebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(middleHebrew2.font.pointSize/14), range: range)
            }
            middleHebrew2.attributedText = formattedM2Hebrew

            if (formattedBottomHebrew.length != 0){
                let range  = bHebrew.range(of: "־")
                formattedBottomHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomHebrew.font.pointSize/14), range: range)
            }
            bottomHebrew.attributedText = formattedBottomHebrew

        }
    }


    func xibSetup() {

        //Set the card type
        cardType = CardType.h

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
