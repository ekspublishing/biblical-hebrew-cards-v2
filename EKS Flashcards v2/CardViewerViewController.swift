//
//  CardViewerViewController.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/11/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class CardViewerViewController: UIViewController , iCarouselDataSource, iCarouselDelegate
{
    let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
    
    var audioPlayer : AVAudioPlayer!
    var reviewChapter :Chapter?;
    
    var workingSet = [Card]();
    var workingSetIndex = -1;
    
    var autoPlayOn = false;
    var msElapsed = 0;
    let tickerInterval = 0.5;
    var timer = Timer.init();

    var autoplayInterval = 5000;
    
    var progressTimer = Timer.init();
    var progressElapsed = 0;
    var progressStep = 0.0166666;
    
    @IBOutlet weak var carousel: iCarousel!
    
    @IBOutlet var addReviewBarButtonItem: UIBarButtonItem!
    @IBOutlet var removeReviewBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var autoPlayBarButtonItem: UIBarButtonItem!
    @IBOutlet var autoplayProgressBar: UIProgressView!
    
    @IBOutlet weak var topToolbar: UIToolbar!
    @IBOutlet weak var bottomToolbar: UIToolbar!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Configure Carousel
        carousel.dataSource = self;
        carousel.delegate = self;
        carousel.type = .linear
        
        configureToolbars();
        
        reviewChapter = appDelegate.catalog?.revewChapter;
        
        self.navigationController!.navigationBar.tintColor = UIColor.black;

        //start AVAudioSession to overide silent mode -- if exception thrown, something is broken
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try! AVAudioSession.sharedInstance().setActive(true)
    }

    func scrollLeft(){
        carousel.scrollToItem(at: carousel.currentItemIndex - 1, duration: 0.400);
    }
    
    func scrollRight(){
        carousel.scrollToItem(at: carousel.currentItemIndex + 1, duration: 0.400);
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        carousel.reloadData();
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated);
        
        self.navigationController!.setNavigationBarHidden(true, animated: true);
        self.navigationController!.interactivePopGestureRecognizer!.isEnabled = false;
        
        appDelegate.buildDeckIfRequired()
        syncWorkingSet();
        
        if appDelegate.isReviewMode {
            bottomToolbar.items?.removeLast();
            bottomToolbar.items?.append(removeReviewBarButtonItem);
        } else {
            bottomToolbar.items?.removeLast();
            bottomToolbar.items?.append(addReviewBarButtonItem);
            
        }
        
        carousel.reloadData();
        
        //work around for layout in wrong orientation / screen lock bug
        self.view.setNeedsLayout();
        self.view.setNeedsDisplay();
        self.view.layoutIfNeeded();

        
        if (workingSet.count != 0 && workingSetIndex > 0){
            carousel.scrollToItem(at: workingSetIndex, animated: animated);
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        validateWorkingSet();
    }
    
    fileprivate func syncWorkingSet(){
    
        //generate the working set and working set index
        workingSet.removeAll();
        let deck = appDelegate.deck!
        if (deck.count > 0){
            for i in 0...deck.count - 1 {
                if (deck[i].isEnabled){
                    workingSet.append(deck[i]);
                    
                    if (i == appDelegate.currentIndex){
                        workingSetIndex = workingSet.count-1;
                    }
                }
            }

            //sync back to take care of the case where the current index is diabled.
            syncAppDelegateFromWorkingSet()
        }
    }
    
    fileprivate func syncAppDelegateFromWorkingSet(){
    
        //Update the app level index from the working set
        if (workingSet.count > 0 && workingSetIndex >= 0){
            let currentCard = workingSet[workingSetIndex] as Card;
            appDelegate.currentIndex = appDelegate.deck!.firstIndex(of: currentCard)!;
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController!.interactivePopGestureRecognizer!.isEnabled = true;
        
        if (autoPlayOn) {stopAutoPlay()}
        
        syncAppDelegateFromWorkingSet();
    }
    
    // Carousel Methods
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        
        var cardView: CardView
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            var cardWidth = carousel.bounds.width * 0.95;
            var cardHeight = cardWidth * 0.66;
            
            while (cardHeight > carousel.bounds.height){
            
                cardWidth = cardWidth * 0.95
                cardHeight = cardWidth * 0.66;
            }
            
            cardView = CardView(frame:CGRect(x:0, y:0, width:cardWidth, height:cardHeight))
                
            cardView.setBorder(false);
        }
        else
        {
            //get a reference to the label in the recycled view
            cardView = view as! CardView;
        }
        
        cardView.setCard(workingSet[index]);

        cardView.addTapHandler(appDelegate.isDoubleClick);
        
        return cardView
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        //update the current index
        if (workingSetIndex != carousel.currentItemIndex){
            workingSetIndex = carousel.currentItemIndex;
            syncAppDelegateFromWorkingSet();
        }
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return workingSet.count;
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return carousel.bounds.width;
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .spacing)
        {
            return value * 1.0
        }
        return value
    }
    
    func carouselDidEndDragging(_ carousel: iCarousel, willDecelerate decelerate: Bool) {
        stopAutoPlay();
    }
    
    func carouselTapped(_ carousel: iCarousel) {
        stopAutoPlay();
    }
    
    //Toolbar Methods
    
    func configureToolbars(){
        
        let twoFingerTap = UITapGestureRecognizer(target: self, action: #selector(CardViewerViewController.toggleToolbars));
        twoFingerTap.numberOfTouchesRequired = 2
        view.addGestureRecognizer(twoFingerTap);
        
        //hack to remove the extra button
        bottomToolbar.items?.removeLast();
        
        bottomToolbar.clipsToBounds = true;
        topToolbar.clipsToBounds = true;
    }
    
    @objc func toggleToolbars(){
        if (topToolbar.alpha == 0.0){
            UIView.animate(withDuration: 1, animations: {
                self.topToolbar.alpha = 1.0
                self.bottomToolbar.alpha = 1.0;
                self.autoplayProgressBar.alpha = 1.0;
            })
        } else {
            UIView.animate(withDuration: 1, animations: {
                self.topToolbar.alpha = 0.0
                self.bottomToolbar.alpha = 0.0;
                self.autoplayProgressBar.alpha = 0.0;
            })
        }
    }

    
    @IBAction func flipCard(_ sender: AnyObject) {
        
        if (carousel.currentItemView != nil) {
            let cardView = carousel.currentItemView as! CardView
            cardView.flip();
            
            if (autoPlayOn) {stopAutoPlay()}
        }
        
    }
    
    @IBAction func playAudio(_ sender: AnyObject) {
        playAudio();

        if (autoPlayOn) {stopAutoPlay()}
    }
    
    func playAudio(){
        let cardView = carousel.currentItemView as! CardView;
        let card = cardView.backingCard;

        do {
            audioPlayer = try AVAudioPlayer(contentsOf: (card!.audioURL)! as URL)
            audioPlayer.play();
        } catch {
            // couldn't load file :(
            print("Card Audio file not found")
        }

     /*   do {
            audioPlayer = try AVAudioPlayer(contentsOf: (card?.audioURL)! as URL, fileTypeHint: nil) } catch _ { return  }
        if (!audioPlayer.isPlaying){
            audioPlayer.currentTime = 0.0;
            audioPlayer.numberOfLoops = 0
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }
         */
    }
    
    
    @IBAction func addReview(_ sender: AnyObject) {
        
        let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
        
        let card = (carousel.currentItemView as! CardView).backingCard;
        
        var alertController :UIAlertController?;
        
        
        if let reviewChapter = appDelegate.catalog?.revewChapter{
        
            if (reviewChapter.contains(card!)){
            
                 alertController = UIAlertController(title: "Card already in Review", message: "This Card is already in the Review Set.", preferredStyle: UIAlertController.Style.alert);
                
            } else {
            
                reviewChapter.addCard(card!);
                alertController = UIAlertController(title: "Card Added To Review", message: "Card successfully added to Review Set.", preferredStyle: UIAlertController.Style.alert);
            }
            
            if (autoPlayOn) {stopAutoPlay()}
            
            alertController!.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
            present(alertController!, animated: true, completion: nil);
 
        }
        
    }
    
    @IBAction func removeReview(_ sender: AnyObject) {
        
        let card = (carousel.currentItemView as! CardView).backingCard;
        
        var alertController :UIAlertController?;
        
        if let reviewChapter = appDelegate.catalog?.revewChapter{
            
            if (reviewChapter.contains(card!)){
                
                reviewChapter.removeCard(card!);
                alertController = UIAlertController(title: "Card Removed From Review", message: "Card successfully removed from the Review Set.", preferredStyle: UIAlertController.Style.alert);
                
                
                //Remove fromt the carousel working set and the deck.
                workingSet.remove(at: carousel.currentItemIndex);
                appDelegate.deck!.remove(at: appDelegate.deck!.firstIndex(of: card!)!);
                
                
                reviewChapter.removeCard(card!);
                carousel.removeItem(at: carousel.currentItemIndex, animated: true);
                
                
                let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) in
                    self.validateWorkingSet();
                    
                }
                
                alertController!.addAction(okAction);
                present(alertController!, animated: true, completion: nil);
                
            }
            
        }
    
    }
    
    @IBAction func autoPlayButtonPressed(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: "AutoPlay", message: "AutoPlay will automatically advance through the deck and flip cards on a set time interveal. The interval can be set in the settings menu.", preferredStyle: UIAlertController.Style.actionSheet);
        
        if (!autoPlayOn){
            alertController.addAction(UIAlertAction(title: "Start AutoPlay", style: UIAlertAction.Style.default){ (alert: UIAlertAction!) in
                self.startAutoPlay();
                });
        } else {
            alertController.addAction(UIAlertAction(title: "Stop AutoPlay", style: UIAlertAction.Style.default){ (alert: UIAlertAction!) in
                self.stopAutoPlay();
                });
        }
    
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = autoPlayBarButtonItem;
        }

        alertController.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default){ (alert: UIAlertAction!) in
            self.performSegue(withIdentifier: "settingsSegue", sender: nil);
        });

        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default){ (alert: UIAlertAction!) in
        });
        
        alertController.view.layoutIfNeeded();
        present(alertController, animated: true, completion: nil);
    
    }
    
    func stopAutoPlay() {
        autoPlayOn = false;
        
        timer.invalidate();
        progressTimer.invalidate();
        autoplayProgressBar.isHidden = true;
    }
    
    func startAutoPlay() {
        autoPlayOn = true;
        autoplayInterval = appDelegate.autoPlayInterval * 1000;
        
        //Unflip the currently loaded views
        for view in carousel.visibleItemViews {
            
            let cardView = view as! CardView
            let card = cardView.backingCard!;
            
            if (card.isFlipped){
                cardView.flip();
            }
        }
        
        //Unflip the whole deck and reload
        for card in workingSet{
            card.isFlipped = false;
        }
        
        //Set the autoplay label
        autoplayProgressBar.isHidden = false;
        autoplayProgressBar.setProgress(1.0, animated: false);
        
        progressTimer = Timer.scheduledTimer(timeInterval: progressStep, target: self, selector: #selector(CardViewerViewController.progressbarUpdate), userInfo: nil, repeats: true);
        progressElapsed = 0;
        
        //Start the overall timer
        timer = Timer.scheduledTimer(timeInterval: tickerInterval, target: self, selector: #selector(CardViewerViewController.tick), userInfo: nil, repeats: true);
        msElapsed = 0;
    }
    
    @objc func progressbarUpdate(){
        
        progressElapsed = progressElapsed + Int(progressStep * 1000)
        let progress = Float(autoplayInterval - progressElapsed)/Float(autoplayInterval);
        autoplayProgressBar.setProgress(progress, animated: false);
    }
    
    @objc func tick(){
        msElapsed = msElapsed + Int(tickerInterval * 1000);
        
        let halfway = autoplayInterval / 2;
        let cardView = carousel.currentItemView as! CardView;
        
        if (msElapsed == halfway){
            cardView.flip();
        } else if (msElapsed == halfway + 1000) {
            playAudio();
        } else if (msElapsed == autoplayInterval){
        
            if (workingSetIndex == workingSet.count - 1){
                stopAutoPlay();
                
                let ac = UIAlertController(title: "End of Deck", message: "AutoPlay has reached the end of the deck.", preferredStyle: UIAlertController.Style.alert);
                let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) in
                    
                }
                ac.addAction(okAction);
                present(ac, animated: true, completion: nil);
            }
            
            //wait for the end of the audio
            if (audioPlayer.isPlaying){
                //if its playing give it an extra second to finish
                msElapsed = msElapsed - 1000;
            } else {
                cardView.flip();
                scrollRight();
                msElapsed = 0;
                progressElapsed = 0;
                autoplayProgressBar.setProgress(1.0, animated: false);
            }
        }
    }
    
    fileprivate func validateWorkingSet(){
        
        if (workingSet.count == 0){
            
            workingSetIndex = 0;
            let ac = UIAlertController(title: "No Cards In Deck", message: "There are no cards in the current deck. Please enable additional cards or chapters, or ensure that Review Only Mode is disabled", preferredStyle: UIAlertController.Style.alert);
            let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) in
                self.performSegue(withIdentifier: "settingsSegue", sender: nil);
                
            }
            ac.addAction(okAction);
            present(ac, animated: true, completion: nil);
        }
    }
    
}
