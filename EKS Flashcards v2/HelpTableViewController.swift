//
//  HelpViewController.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class HelpTableViewController: UITableViewController {
    
    var htmlDictionary :NSDictionary?;
    var keys :NSArray?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if (htmlDictionary == nil)
        {
            #if PHCARDS
                var path =  Bundle.main.path(forResource: "PHHelpHtml", ofType: "plist")
            #else
                var path =  Bundle.main.path(forResource: "BHHelpHtml", ofType:"plist");
            #endif
            htmlDictionary = NSDictionary.init(contentsOfFile: path!);
            path = Bundle.main.path(forResource: "HelpKeys", ofType: "plist");
            keys = NSArray.init(contentsOfFile: path!)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return keys!.count;
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "helpViewCell", for: indexPath)
        cell.textLabel!.text = keys![indexPath.row] as? String;
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toDetail"){
            let helpDetailController = segue.destination as! HelpDetailViewController;
            let path = tableView.indexPathForSelectedRow!;
            
            let rowKey = keys![path.row] as! String;
            helpDetailController.html = htmlDictionary![rowKey] as? String;
            helpDetailController.navTitle = rowKey;
        
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
