//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 3/4/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardR2 :Card {

    var backHebrew :String;
    var backEnglish :String
    var topItalics :String;
    var bottomItalics :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, backHebrew :String, backEnglish :String, topItalics :String, bottomItalics :String, redCoding :String) {
        self.backEnglish = backEnglish;
        self.backHebrew = backHebrew;
        self.topItalics = topItalics;
        self.bottomItalics = bottomItalics

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: redCoding);

    }

    required init(coder aDecoder: NSCoder) {


        self.backHebrew = aDecoder.decodeObject(forKey: "backHebrew") as! String;
        self.backEnglish = aDecoder.decodeObject(forKey: "backEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        self.bottomItalics = aDecoder.decodeObject(forKey: "bottomItalics") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(backHebrew, forKey: "backHebrew");
        aCoder.encode(backEnglish, forKey: "backEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        aCoder.encode(bottomItalics, forKey: "bottomItalics");
        
        super.encode(with: aCoder);
        
        
    }
    
}
