//
//  Catalog.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

open class Catalog : NSObject, NSCoding {

    #if PHCARDS
    static let fontName = "Palatino";
    #else
    static let fontName = "NewPeninimMT";
    #endif

    static let path =  Bundle.main.path(forResource: "charMapping", ofType:"plist")!;
    static let charDict = NSDictionary.init(contentsOfFile: path);


    var chapters = [Chapter]();
    var revewChapter = Chapter(title: "Review Chapter", description: "Set of cards marked for review")


    var title :String

    init(title :String) {

        self.title = title;
        self.revewChapter.isReview = true;
        self.revewChapter.isEnabled = false;
    }

    //Obviously this is an ugly hack to unblock development of the menu and settings.
    static func loadCatalog() -> Catalog {

        #if PHCARDS
            return loadPHCatalog();
        #else
            return loadBHCatalog();
        #endif

    }

    //Obviously this is an ugly hack to unblock development of the menu and settings.
    static func loadPHCatalog() -> Catalog {

        let catalog = Catalog(title: "EKS Prayerbook Hebrew Flashcards");

        let enabledCards = ["A","A1","B","B1","B2","E1","R1","R2","R3","R4", "R5"];

        let catalogLoader = CatalogLoader();

        catalog.revewChapter.cards.removeAll();

        let path = Bundle.main.path(forResource: "prayerbook_flashcards", ofType: "csv");
        let parser = CHCSVParser(contentsOfCSVURL: URL(fileURLWithPath: path!));

        parser?.delegate = catalogLoader;
        parser?.parse();

        var knownChapters = Dictionary<Int,Chapter>();

        for cardDictionary in catalogLoader.cards {
            let parsedChapterNumber = Int(cardDictionary["Chapter"]!);
            var chapter = knownChapters[parsedChapterNumber!];

            if (chapter == nil){
                chapter = Chapter(title: String(format: "Chapter %i", parsedChapterNumber!), description: "")
                knownChapters[parsedChapterNumber!] = chapter;
                catalog.chapters.append(chapter!);
            }

            let cardNumber = Int(cardDictionary["Number"]!)!;
            var card :Card?;

            if (cardDictionary["Type"] == "A"){

                card = CardA(
                    type: CardType.a,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    backItalics: cardDictionary["Italic 1"]!);

            }else if (cardDictionary["Type"] == "A1"){

                card = CardA1(
                    type: CardType.a1,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    backItalics: cardDictionary["Italic 1"]!);

            }else if (cardDictionary["Type"] == "B" || cardDictionary["Type"] == "B1"){

                card = CardB(
                    type: CardType.b,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    leftHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftEnglish: cardDictionary["Back English 1"]!,
                    leftItalics: cardDictionary["Italic 1"]!,
                    rightHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightEnglish: cardDictionary["Back English 2"]!,
                    rightItalics: cardDictionary["Italic 2"]!);


            }else if (cardDictionary["Type"] == "B2"){

                card = CardB2(
                    type: CardType.b2,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    leftHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftItalics: cardDictionary["Italic 1"]!,
                    rightHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightItalics: cardDictionary["Italic 2"]!);

            }else if (cardDictionary["Type"] == "E1"){

                card = CardE1(
                    type: CardType.e1,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    leftTopHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftTopItalics: cardDictionary["Italic 1"]!,
                    leftBottomHebrew: cardDictionary["Back Hebrew 3"]!,
                    leftBottomItalics: cardDictionary["Italic 3"]!,
                    rightTopHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightTopItalics: cardDictionary["Italic 2"]!,
                    rightBottomHebrew: cardDictionary["Back Hebrew 4"]!,
                    rightBottomItalics: cardDictionary["Italic 4"]!)

            }else if (cardDictionary["Type"] == "R1"){

                card = CardR1(
                    type: CardType.r1,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    backItalics: cardDictionary["Italic 1"]!,
                    redCoding: cardDictionary["Red Coding"]!);

            }else if (cardDictionary["Type"] == "R2"){


                card = CardR2(
                    type: CardType.r2,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    bottomItalics: cardDictionary["Italic 2"]!,
                    redCoding: cardDictionary["Red Coding"]!);

            }else if (cardDictionary["Type"] == "R3"){

                card = CardR3(
                    type: CardType.r3,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topHebrew: cardDictionary["Back Hebrew 1"]!,
                    bottomHebrew: cardDictionary["Back Hebrew 2"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    backItalics: cardDictionary["Italic 1"]!,
                    redCoding: cardDictionary["Red Coding"]!);


            }else if (cardDictionary["Type"] == "R4"){

                card = CardR4(
                    type: CardType.r4,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topHebrew: cardDictionary["Back Hebrew 1"]!,
                    bottomHebrew: cardDictionary["Back Hebrew 2"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    bottomItalics: cardDictionary["Italic 2"]!,
                    redCoding: cardDictionary["Red Coding"]!);


            }else if (cardDictionary["Type"] == "R5"){


                card = CardR5(
                    type: CardType.r5,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    bottomEnglish: cardDictionary["Back English 2"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    middleItalics: cardDictionary["Italic 2"]!,
                    bottomItalics: cardDictionary["Italic 3"]!,
                    redCoding: cardDictionary["Red Coding"]!)

            }

            if (enabledCards.contains(cardDictionary["Type"]!) && card != nil){
                chapter?.addCard(card!);
            }
        }

        return catalog;

    }


    //Obviously this is an ugly hack to unblock development of the menu and settings.
    static func loadBHCatalog() -> Catalog {

        let catalog = Catalog(title: "EKS Biblical Hebrew Flashcards");

        let catalogLoader = CatalogLoader();

        catalog.revewChapter.cards.removeAll();

        let path = Bundle.main.path(forResource: "bible_flashcards", ofType: "csv");
        let parser = CHCSVParser(contentsOfCSVURL: URL(fileURLWithPath: path!));

        parser?.delegate = catalogLoader;
        parser?.parse();

        var knownChapters = Dictionary<Int,Chapter>();

        for cardDictionary in catalogLoader.cards {
            let parsedChapterNumber = Int(cardDictionary["Chapter"]!);
            var chapter = knownChapters[parsedChapterNumber!];

            if (chapter == nil){
                chapter = Chapter(title: String(format: "Chapter %i", parsedChapterNumber!), description: "")
                knownChapters[parsedChapterNumber!] = chapter;
                catalog.chapters.append(chapter!);
            }

            if (cardDictionary["Type"] == "A"){

                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardA(
                    type: CardType.a,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    backHebrew: cardDictionary["Back Hebrew 1"]!,
                    backEnglish: cardDictionary["Back English 1"]!,
                    backItalics: cardDictionary["Italic 1"]!);

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "B"){

                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardB(
                    type: CardType.b,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    leftHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftEnglish: cardDictionary["Back English 1"]!,
                    leftItalics: cardDictionary["Italic 1"]!,
                    rightHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightEnglish: cardDictionary["Back English 2"]!,
                    rightItalics: cardDictionary["Italic 2"]!);


                chapter?.addCard(card);
            } else if (cardDictionary["Type"] == "C"){

                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardC(type: CardType.c, numericalIndex: cardNumber, alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!, frontHebrew: cardDictionary["Front"]!, chapterDescription: cardDictionary["Chapter Description"]!, leftHebrew: cardDictionary["Back Hebrew 1"]!, rightHebrew: cardDictionary["Back Hebrew 2"]!, bottomHebrew: cardDictionary["Back Hebrew 3"]!, topEnglish: cardDictionary["Back English 1"]!, topItalics: cardDictionary["Italic 1"]!, bottomEnglish: cardDictionary["Back English 2"]!)

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "D"){
                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardD(
                    type: CardType.d,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    bottomEnglish: cardDictionary["Back English 2"]!,
                    bottomItalics: cardDictionary["Italic 2"]!,
                    leftTopHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftBottomHebrew: cardDictionary["Back Hebrew 3"]!,
                    rightTopHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightBottomHebrew: cardDictionary["Back Hebrew 4"]!)

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "E"){
                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardE(
                    type: CardType.e,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    leftTopHebrew: cardDictionary["Back Hebrew 1"]!,
                    leftBottomHebrew: cardDictionary["Back Hebrew 3"]!,
                    rightTopHebrew: cardDictionary["Back Hebrew 2"]!,
                    rightBottomHebrew: cardDictionary["Back Hebrew 4"]!)

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "F1"){
                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardF1(
                    type: CardType.f1,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    leftHebrew: cardDictionary["Back Hebrew 1"]!,
                    rightHebrew: cardDictionary["Back Hebrew 2"]!)

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "F2"){
                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardF2(
                    type: CardType.f2,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    topItalics: cardDictionary["Italic 1"]!,
                    topHebrew: cardDictionary["Back Hebrew 1"]!,
                    bottomHebrew: cardDictionary["Back Hebrew 2"]!)

                chapter?.addCard(card);

            } else if (cardDictionary["Type"] == "H"){
                let cardNumber = Int(cardDictionary["Number"]!)!;

                let card = CardH(
                    type: CardType.h,
                    numericalIndex: cardNumber,
                    alphabeticalIndex: Int(cardDictionary["Alphabetical"]!)!,
                    frontHebrew: cardDictionary["Front"]!,
                    chapterDescription: cardDictionary["Chapter Description"]!,
                    mainHebrew: cardDictionary["Back Hebrew 1"]!,
                    topHebrew: cardDictionary["Back Hebrew 2"]!,
                    middleHebrew1: cardDictionary["Back Hebrew 3"]!,
                    middleHebrew2: cardDictionary["Back Hebrew 4"]!,
                    bottomHebrew: cardDictionary["Back Hebrew 5"]!,
                    topEnglish: cardDictionary["Back English 1"]!,
                    middleEnglish1: cardDictionary["Back English 2"]!,
                    middleEnglish2: cardDictionary["Back English 3"]!,
                    bottomEnglish: cardDictionary["Back English 4"]!)

                chapter?.addCard(card);
            }
        }
        
        return catalog;
        
    }
    
    required public init(coder aDecoder: NSCoder) {
        
        self.chapters = aDecoder.decodeObject(forKey: "chapters") as! [Chapter];
        self.revewChapter = aDecoder.decodeObject(forKey: "revewChapter") as! Chapter;
        self.title = aDecoder.decodeObject(forKey: "title") as! String;
        
    }
    
    open func encode(with aCoder: NSCoder) {
        
        aCoder.encode(chapters, forKey: "chapters");
        aCoder.encode(revewChapter, forKey: "revewChapter");
        aCoder.encode(title, forKey: "title");
        
    }
    
}
