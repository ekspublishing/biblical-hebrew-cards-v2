//
//  CardTypeD.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 1/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardF2 :Card {
    
    
    var topEnglish :String;
    var topItalics :String;
    
    var topHebrew :String;
    var bottomHebrew :String;
    
    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String,
        topEnglish :String, topItalics :String, topHebrew :String, bottomHebrew :String){
            
            self.topEnglish = topEnglish
            self.topItalics = topItalics
            
            self.topHebrew = topHebrew
            self.bottomHebrew = bottomHebrew
            
            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        
        self.topHebrew = aDecoder.decodeObject(forKey: "topHebrew") as! String;
        self.bottomHebrew = aDecoder.decodeObject(forKey: "bottomHebrew") as! String;
        
        super.init(coder: aDecoder);
        
    }
    
    override func encode(with aCoder: NSCoder) {
        
        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        
        aCoder.encode(topHebrew, forKey: "topHebrew");
        aCoder.encode(bottomHebrew, forKey: "bottomHebrew");
        
        super.encode(with: aCoder);
    }
}
