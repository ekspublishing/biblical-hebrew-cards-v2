//
//  Chapter.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

open class Chapter: NSObject, NSCoding {

    var cards = [Card]();
    var title :String;
    var chapterDescription :String;
    var isEnabled = true;
    var isReview = false;
    
    init (title :String, description :String){
    
        self.title = title;
        self.chapterDescription = description;
        
    }
    
    func contains(_ card :Card) -> Bool {
        
        return cards.contains(card);
        
    }
    
    func addCard(_ card :Card) {
    
        if (!cards.contains(card)){
            cards.append(card);
        }
    }
    
    func removeCard(_ card :Card){
    
        let index = cards.firstIndex(of: card);
        if (index != nil) {
            cards.remove(at: index!);
        }
    }
    
    func count() -> Int {
        return cards.count;
    }
    
    func cardAt(_ index :Int) -> Card {
        return cards[index];
    }
    
    required public init(coder aDecoder: NSCoder) {
        
        self.cards = aDecoder.decodeObject(forKey: "cards") as! [Card];
        
        self.title = aDecoder.decodeObject(forKey: "title") as! String;
        self.chapterDescription = aDecoder.decodeObject(forKey: "chapterDescription") as! String;
        
        self.isReview = aDecoder.decodeBool(forKey: "isReview");
        self.isEnabled = aDecoder.decodeBool(forKey: "isEnabled");
    }
    
    open func encode(with aCoder: NSCoder) {
        
        aCoder.encode(cards, forKey: "cards");
        aCoder.encode(title, forKey: "title");
        aCoder.encode(chapterDescription, forKey: "chapterDescription");
        aCoder.encode(isEnabled, forKey: "isEnabled");
        aCoder.encode(isReview, forKey: "isReview");
        
    }
    
}
