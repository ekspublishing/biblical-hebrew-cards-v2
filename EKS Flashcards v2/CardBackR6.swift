
import UIKit

class CardBackR6: RedCardFace {

    let nibName = "CardBackR6"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var topEnglishLabel: UILabel!
    @IBOutlet weak var bottomEnglishLabel: UILabel!
    @IBOutlet weak var hebrewLabel: UILabel!
    @IBOutlet weak var italicsLabel: UILabel!


    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        let font = UIFont(name: Catalog.fontName, size: 30);
        hebrewLabel.font = font;

        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat(rootView.frame.size.width) / 12;
        topEnglishLabel.font = topEnglishLabel.font.fontWithSize(fontSize);
        bottomEnglishLabel.font = bottomEnglishLabel.font.fontWithSize(fontSize);

        //Hebrew font is weirdly small
        hebrewLabel.font = hebrewLabel.font.fontWithSize(fontSize * 1.5);

        fontSize = CGFloat(rootView.frame.size.width) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.fontWithSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.fontWithSize(fontSize);

        fontSize = CGFloat(rootView.frame.size.width) / 20
        italicsLabel.font = italicsLabel.font.fontWithSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = rootView.frame.size.width < 300;

        numericalIndexLabel.hidden = hideSmallViews;
        chapterDescriptionLabel.hidden = hideSmallViews;

        applyFormatting()

    }

    private func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let r6Card = backingCard as! CardR6

            let hebrew = r6Card.backHebrew as NSString

            let formattedHebrew = NSMutableAttributedString(string: hebrew as String)
            if (formattedHebrew != ""){
                let range = hebrew.rangeOfString("־")
                formattedHebrew.addAttribute(NSBaselineOffsetAttributeName, value: -(hebrewLabel.font.pointSize/14), range: range)
            }
            hebrewLabel.attributedText = formattedHebrew
        }
    }

    private func applyFormatting(){

        if (backingCard != nil){
            let r6Card = backingCard as! CardR6

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()
            
            //the trailing space is to handle a clipping issue.
            var combo1 = r6Card.topEnglish + " " + r6Card.topItalics + " " as NSString;
            var combo2 = r6Card.bottomEnglish + " " + r6Card.middleItalics + " " as NSString;

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"
            combo1 = combo1.stringByReplacingOccurrencesOfString("\\n", withString: myNewLineStr)
            combo2 = combo2.stringByReplacingOccurrencesOfString("\\n", withString: myNewLineStr)

            var combinedString = NSMutableAttributedString(string: combo1 as String);
            if (r6Card.topItalics != ""){
                let range = combo1.rangeOfString(r6Card.topItalics, options: NSStringCompareOptions.BackwardsSearch)
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglishLabel.font.pointSize/2)
                combinedString.addAttribute(NSFontAttributeName, value: itFont!, range: range)
            }
            topEnglishLabel.attributedText = combinedString;

            combinedString = NSMutableAttributedString(string: combo2 as String);
            if (r6Card.middleItalics != ""){
                let range = combo2.rangeOfString(r6Card.middleItalics, options: NSStringCompareOptions.BackwardsSearch)
                let itFont = UIFont(name: "HoeflerText-Italic", size: bottomEnglishLabel.font.pointSize/2)
                combinedString.addAttribute(NSFontAttributeName, value: itFont!, range: range)
            }
            bottomEnglishLabel.attributedText = combinedString;

            applyRed(r6Card.backHebrew, redIndexString: r6Card.redCoding, targetMutableString: &hebrewLabel.attributedText!)
        }
    }

    override func setCard(card :Card){

        if let card_R6 = card as? CardR6 {
            chapterDescriptionLabel.text = card_R6.chapterDescription;
            topEnglishLabel.text = card_R6.topEnglish;
            bottomEnglishLabel.text = card_R6.bottomEnglish;
            hebrewLabel.text = card_R6.backHebrew;
            italicsLabel.text = card_R6.bottomItalics
            numericalIndexLabel.text = String(card_R6.numericalIndex);
            backingCard = card;

            applyFormatting()
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.R5

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }

    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
}
