//
//  CardBackE.swift
//  EKS Flashcards
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackF2: CardFace {

    let nibName = "CardBackF2"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var topEnglish: UILabel!
    @IBOutlet var topHebrew: UILabel!
    @IBOutlet var bottomHebrew: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        topHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        bottomHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;

        topHebrew.font = topHebrew.font.withSize(fontSize * 2);
        bottomHebrew.font = bottomHebrew.font.withSize(fontSize * 2);

        topEnglish.font = topEnglish.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);


        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting();

    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let f2Card = backingCard as! CardF2

            //format the maqaf in the hebrew if there is one
            hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            let combo = f2Card.topEnglish + " " + f2Card.topItalics + " " as NSString;

            let combinedString = NSMutableAttributedString(string: combo as String);
            if (f2Card.topItalics != ""){
                let range = combo.range(of: f2Card.topItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglish.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            topEnglish.attributedText = combinedString;
        }

    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let f2Card = backingCard as! CardF2

            let tHebrew = f2Card.topHebrew as NSString
            let bHebrew = f2Card.bottomHebrew as NSString

            let formattedTopHebrew = NSMutableAttributedString(string: tHebrew as String)
            let formattedBottomHebrew = NSMutableAttributedString(string: bHebrew as String)

            if (formattedTopHebrew.length != 0){
                let range = tHebrew.range(of: "־")
                formattedTopHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(topHebrew.font.pointSize/14), range: range)
            }
            topHebrew.attributedText = formattedTopHebrew

            if (formattedBottomHebrew.length != 0){
                let range = bHebrew.range(of: "־")
                formattedBottomHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomHebrew.font.pointSize/14), range: range)
            }
            bottomHebrew.attributedText = formattedBottomHebrew

        }
    }

    override func setCard(_ card :Card){

        if let card_F2 = card as? CardF2 {
            chapterDescriptionLabel.text = card_F2.chapterDescription;

            self.topEnglish.text = card_F2.topEnglish
            self.topHebrew.text = card_F2.topHebrew
            self.bottomHebrew.text = card_F2.bottomHebrew
            numericalIndexLabel.text = String(card_F2.numericalIndex);
            backingCard = card;

            applyFormatting();
        }

    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.f2

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
