//
//  CardBackE.swift
//  EKS Flashcards
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackF1: CardFace {

    let nibName = "CardBackF1"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var topEnglish: UILabel!

    @IBOutlet var leftHebrew: UILabel!

    @IBOutlet var rightHebrew: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        leftHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        rightHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;


        leftHebrew.font = leftHebrew.font.withSize(fontSize * 2);
        rightHebrew.font = rightHebrew.font.withSize(fontSize * 2);

        topEnglish.font = topEnglish.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        applyFormatting();

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);
        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

    }

    override func setCard(_ card :Card){

        if let card_F1 = card as? CardF1 {

            backingCard = card_F1;

            chapterDescriptionLabel.text = card_F1.chapterDescription;

            self.topEnglish.text = card_F1.topEnglish

            self.leftHebrew.text = card_F1.leftHebrew
            self.rightHebrew.text = card_F1.rightHebrew

            numericalIndexLabel.text = String(card_F1.numericalIndex);

            applyFormatting();        }

    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let f1Card = backingCard as! CardF1

            //format the maqaf in the hebrew if there is one
            hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            let combo = f1Card.topEnglish + " " + f1Card.topItalics + " " as NSString;

            let combinedString = NSMutableAttributedString(string: combo as String);
            if (f1Card.topItalics != ""){
                let range = combo.range(of: f1Card.topItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglish.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            topEnglish.attributedText = combinedString;
        }

    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let f1Card = backingCard as! CardF1

            let lHebrew = f1Card.leftHebrew as NSString
            let rHebrew = f1Card.rightHebrew as NSString

            let formattedLeftHebrew = NSMutableAttributedString(string: lHebrew as String)
            let formattedRightHebrew = NSMutableAttributedString(string: rHebrew as String)

            if (formattedLeftHebrew.length != 0){
                let range = lHebrew.range(of: "־")
                formattedLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftHebrew.font.pointSize/14), range: range)
            }
            leftHebrew.attributedText = formattedLeftHebrew

            if (formattedRightHebrew.length != 0){
                let range = rHebrew.range(of: "־")
                formattedRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightHebrew.font.pointSize/14), range: range)
            }
            rightHebrew.attributedText = formattedRightHebrew
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.f1

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
