//
//  CardTypeD.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardD :Card {
    
    
    var topEnglish :String;
    var topItalics :String;
    
    var bottomEnglish :String;
    var bottomItalics :String;
    
    var leftTopHebrew :String;
    var leftBottomHebrew :String;
    
    var rightTopHebrew :String;
    var rightBottomHebrew :String;
    
    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String,
        topEnglish :String, topItalics :String, bottomEnglish :String, bottomItalics :String, leftTopHebrew :String,
        leftBottomHebrew :String, rightTopHebrew :String, rightBottomHebrew :String){
            
            self.topEnglish = topEnglish
            self.topItalics = topItalics
            
            self.bottomEnglish = bottomEnglish
            self.bottomItalics = bottomItalics
            
            self.leftTopHebrew = leftTopHebrew
            self.leftBottomHebrew = leftBottomHebrew
            
            self.rightTopHebrew = rightTopHebrew
            self.rightBottomHebrew = rightBottomHebrew
            
            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }

    required init(coder aDecoder: NSCoder) {
        
        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        
        self.bottomEnglish = aDecoder.decodeObject(forKey: "bottomEnglish") as! String;
        self.bottomItalics = aDecoder.decodeObject(forKey: "bottomItalics") as! String;
        
        self.leftTopHebrew = aDecoder.decodeObject(forKey: "leftTopHebrew") as! String;
        self.leftBottomHebrew = aDecoder.decodeObject(forKey: "leftBottomHebrew") as! String;
        
        self.rightTopHebrew = aDecoder.decodeObject(forKey: "rightTopHebrew") as! String;
        self.rightBottomHebrew = aDecoder.decodeObject(forKey: "rightBottomHebrew") as! String;
        
        super.init(coder: aDecoder);
        
    }
    
    override func encode(with aCoder: NSCoder) {
        
        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        
        aCoder.encode(bottomEnglish, forKey: "bottomEnglish");
        aCoder.encode(bottomItalics, forKey: "bottomItalics");
        
        aCoder.encode(leftTopHebrew, forKey: "leftTopHebrew");
        aCoder.encode(leftBottomHebrew, forKey: "leftBottomHebrew");
        
        aCoder.encode(rightTopHebrew, forKey: "rightTopHebrew");
        aCoder.encode(rightBottomHebrew, forKey: "rightBottomHebrew");
        
        super.encode(with: aCoder);
    }
}
