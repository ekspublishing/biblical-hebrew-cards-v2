
//
//  DeckViewTableViewCell.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/9/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class DeckViewTableViewCell: UITableViewCell {

    var backingCard :Card?

    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var currentIndexIndicator: UIImageView!
    @IBOutlet weak var inReviewLabel: UILabel!

    @IBOutlet var enabledSwitch: UISwitch!
    @IBOutlet var cardLabelUnderline: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCard(_ card :Card){

        self.backingCard = card;
        cardLabel.text = "Card " + String(card.numericalIndex)
        cardView.setCard(card);

        //set some borders in the deck view
        cardView.setBorder(true);
    }

    func setCurrentIndex(_ isCurrentIndex :Bool, animated :Bool){

        let selectedBorderColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1).cgColor;

        if (!animated){
            if (isCurrentIndex){
                self.cardView.layer.borderColor = selectedBorderColor;
                self.cardView.layer.borderWidth = 4;
                cardLabelUnderline.layer.backgroundColor = selectedBorderColor

            } else {
                self.cardView.layer.borderColor = UIColor.black.cgColor;
                self.cardView.layer.borderWidth = 0;
                cardLabelUnderline.layer.backgroundColor = UIColor.white.cgColor
            }
        } else {
            let borderColor = CABasicAnimation(keyPath: "borderColor");
            let borderWidth = CABasicAnimation(keyPath: "borderWidth");
            let underlineColor = CABasicAnimation(keyPath: "underlineColor")

            if (isCurrentIndex) {

                borderColor.fromValue = UIColor.black.cgColor
                borderColor.toValue = selectedBorderColor;
                self.cardView.layer.borderColor = selectedBorderColor;

                borderWidth.fromValue = 1
                borderWidth.toValue = 4
                self.cardView.layer.borderWidth = 4;

                underlineColor.fromValue = UIColor.white.cgColor
                underlineColor.toValue = selectedBorderColor
                self.cardLabelUnderline.layer.backgroundColor = selectedBorderColor;

            } else {

                borderColor.fromValue = selectedBorderColor;
                borderColor.toValue = UIColor.black.cgColor;
                self.cardView.layer.borderColor = UIColor.black.cgColor;

                borderWidth.fromValue = 4
                borderWidth.toValue = 1
                self.cardView.layer.borderWidth = 0;

                underlineColor.fromValue = selectedBorderColor
                underlineColor.toValue = UIColor.white.cgColor
                self.cardLabelUnderline.layer.backgroundColor = UIColor.white.cgColor;

            }

            let both = CAAnimationGroup();
            both.duration = 0.3;
            both.animations = [borderColor, borderWidth, underlineColor];
            both.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear);

            self.cardView.layer.add(both, forKey: "color and width");
        }
    }

    func setDisabled(_ isDisabled :Bool, animated :Bool){

        if(isDisabled){

            if (animated) {
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.cardView.alpha = 0.3; self.cardLabel.alpha = 0.3; self.inReviewLabel.alpha = 0.8
                    }, completion: nil);
            } else {
                self.cardView.alpha = 0.3
                self.cardLabel.alpha = 0.3
                self.inReviewLabel.alpha = 0.8
            }

        } else {
            if (animated) {
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    self.cardView.alpha = 1.0; self.cardLabel.alpha = 1.0; self.inReviewLabel.alpha = 1.0
                    }, completion: nil);
            } else {
                self.cardView.alpha = 1.0
                self.cardLabel.alpha = 1.0
                self.inReviewLabel.alpha = 1.0
            }
        }
    }


    @IBAction func enabledSwitchChanged(_ sender: UISwitch) {
        backingCard?.isEnabled = sender.isOn;
        setDisabled(!sender.isOn, animated: true);
        
    }
}
