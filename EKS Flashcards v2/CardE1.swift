//
//  CardTypeD.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 1/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardE1 :Card {


    var topEnglish :String;

    var leftTopHebrew :String;
    var leftTopItalics :String;
    var leftBottomHebrew :String;
    var leftBottomItalics :String;

    var rightTopHebrew :String;
    var rightTopItalics :String;
    var rightBottomHebrew :String;
    var rightBottomItalics :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String,
        topEnglish :String, leftTopHebrew :String, leftTopItalics :String, leftBottomHebrew :String, leftBottomItalics :String, rightTopHebrew :String, rightTopItalics :String, rightBottomHebrew :String, rightBottomItalics :String){

            self.topEnglish = topEnglish

            self.leftTopHebrew = leftTopHebrew
            self.leftTopItalics = leftTopItalics
            self.leftBottomHebrew = leftBottomHebrew
            self.leftBottomItalics = leftBottomItalics

            self.rightTopHebrew = rightTopHebrew
            self.rightTopItalics = rightTopItalics
            self.rightBottomHebrew = rightBottomHebrew
            self.rightBottomItalics = rightBottomItalics

            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }

    required init(coder aDecoder: NSCoder) {

        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;

        self.leftTopHebrew = aDecoder.decodeObject(forKey: "leftTopHebrew") as! String;
        self.leftTopItalics = aDecoder.decodeObject(forKey: "leftTopItalics") as! String;
        self.leftBottomHebrew = aDecoder.decodeObject(forKey: "leftBottomHebrew") as! String;
        self.leftBottomItalics = aDecoder.decodeObject(forKey: "leftBottomItalics") as! String;

        self.rightTopHebrew = aDecoder.decodeObject(forKey: "rightTopHebrew") as! String;
        self.rightTopItalics = aDecoder.decodeObject(forKey: "rightTopItalics") as! String;
        self.rightBottomHebrew = aDecoder.decodeObject(forKey: "rightBottomHebrew") as! String;
        self.rightBottomItalics = aDecoder.decodeObject(forKey: "rightBottomItalics") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(topEnglish, forKey: "topEnglish");

        aCoder.encode(leftTopHebrew, forKey: "leftTopHebrew");
        aCoder.encode(leftTopItalics, forKey: "leftTopItalics")
        aCoder.encode(leftBottomHebrew, forKey: "leftBottomHebrew");
        aCoder.encode(leftBottomItalics, forKey: "leftBottomItalics")

        aCoder.encode(rightTopHebrew, forKey: "rightTopHebrew");
        aCoder.encode(rightTopItalics, forKey: "rightTopItalics")
        aCoder.encode(rightBottomHebrew, forKey: "rightBottomHebrew");
        aCoder.encode(rightBottomItalics, forKey: "rightBottomItalics")
        
        super.encode(with: aCoder);
    }
}
