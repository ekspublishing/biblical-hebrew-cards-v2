//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardView: UIView {

    var backingCard :Card?
    var frontCardFace :CardFace?;
    var backCardFace :CardFace?;
    var borderOn = false;

    override init(frame: CGRect) {
        super.init(frame: frame);
        childViewSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        childViewSetup();
    }

    @objc func flip() {


        if let theCard  = backingCard {
            if (!theCard.isFlipped){

                let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
                UIView.transition(from: self.frontCardFace!, to: self.backCardFace!, duration: 1.0, options: transitionOptions)

            } else {
                let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromLeft, .showHideTransitionViews]

                UIView.transition(from: self.backCardFace!, to: self.frontCardFace!, duration: 1.0, options: transitionOptions)
            }

            theCard.isFlipped = !theCard.isFlipped;

        }
    }

    func setCard(_ card :Card){

        self.backingCard = card;
        frontCardFace?.setCard(card);


        if(backCardFace?.cardType != card.type){
            backCardFace?.removeFromSuperview();
            backCardFace = getBackFace(card.type);
            configureFace(backCardFace!);
            addSubview(backCardFace!)

            setBorder(self.borderOn);
        }

        backCardFace?.setCard(card);

        //ensure the right face is showing
        frontCardFace?.isHidden = card.isFlipped;
        backCardFace?.isHidden = !card.isFlipped;

        //TODO will need to recreate the back face if the incoming card doesnt match the old one
    }

    func setBorder(_ isOn :Bool){

        if (isOn){

            borderOn = true;

            frontCardFace!.layer.borderColor = UIColor.black.cgColor;
            frontCardFace!.layer.borderWidth = 2.0;

            backCardFace!.layer.borderColor = UIColor.black.cgColor;
            backCardFace!.layer.borderWidth = 2.0;

        } else {

            borderOn = false;

            frontCardFace!.layer.borderWidth = 0.0;
            backCardFace!.layer.borderWidth = 0.0;

        }

    }

    func childViewSetup() {

        frontCardFace = CardFront(frame: self.bounds);
        configureFace(frontCardFace!);
        addSubview(frontCardFace!)

        backCardFace = getBackFace(CardType.a);
        configureFace(backCardFace!);
        backCardFace?.isHidden = true;
        addSubview(backCardFace!)
        addTapHandler(true);

    }

    func addTapHandler(_ isDoubleClick :Bool){

        let tap = UITapGestureRecognizer(target: self, action: #selector(CardView.flip))
        if (isDoubleClick){
            tap.numberOfTapsRequired = 2
        } else {
            tap.numberOfTapsRequired = 1
        }
        tap.cancelsTouchesInView = false;
        self.addGestureRecognizer(tap);

    }

    func getBackFace(_ type :CardType) -> CardFace {

        switch type {

        case .a:
            return CardBackA(frame: self.bounds);
        case .b:
            return CardBackB(frame: self.bounds);
        case .c:
            return CardBackC(frame: self.bounds);
        case .d:
            return CardBackD(frame: self.bounds);
        case .e:
            return CardBackE(frame: self.bounds);
        case .f1:
            return CardBackF1(frame: self.bounds);
        case .f2:
            return CardBackF2(frame: self.bounds);
        case .h:
            return CardBackH(frame: self.bounds);
        case .a1:
            return CardBackA1(frame: self.bounds);
        case .b2:
            return CardBackB2(frame: self.bounds);
        case .e1:
            return CardBackE1(frame: self.bounds);
        case .r1:
            return CardBackR1(frame: self.bounds);
        case .r2:
            return CardBackR2(frame: self.bounds);
        case .r3:
            return CardBackR3(frame: self.bounds);
        case .r4:
            return CardBackR4(frame: self.bounds);
        case .r5:
            return CardBackR5(frame: self.bounds);
        }

    }

    func configureFace(_ face :UIView){

        face.frame = bounds
        face.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
    }
    
    
}
