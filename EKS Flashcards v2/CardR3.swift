//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 3/4/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardR3 :Card {

    var topHebrew :String;
    var bottomHebrew :String;
    var backEnglish :String
    var backItalics :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, topHebrew :String, bottomHebrew :String, backEnglish :String, backItalics :String, redCoding :String) {
        self.backEnglish = backEnglish;
        self.topHebrew = topHebrew;
        self.bottomHebrew = bottomHebrew
        self.backItalics = backItalics;

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: redCoding);

    }

    required init(coder aDecoder: NSCoder) {


        self.topHebrew = aDecoder.decodeObject(forKey: "topHebrew") as! String;
        self.bottomHebrew = aDecoder.decodeObject(forKey: "bottomHebrew") as! String;
        self.backEnglish = aDecoder.decodeObject(forKey: "backEnglish") as! String;
        self.backItalics = aDecoder.decodeObject(forKey: "backItalics") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(topHebrew, forKey: "topHebrew");
        aCoder.encode(bottomHebrew, forKey: "bottomHebrew");
        aCoder.encode(backEnglish, forKey: "backEnglish");
        aCoder.encode(backItalics, forKey: "backItalics");

        super.encode(with: aCoder);
        
        
    }
    
}
