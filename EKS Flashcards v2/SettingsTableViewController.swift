//
//  SettingsTableViewController.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate

    var autoplayIntervals = [4,5,6,7,8,9,10,15,20,30];

    let kChapterSelectionRow = 0;
    let kFlashcardSortingRow = 1;
    let kReviewOnlyModeRow = 2;
    let kAutoplayRow = 3;
    let kDoubleClickRow = 4;
    let kResetDefaultsRow = 5;


    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData();

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if (indexPath.row == kChapterSelectionRow){
            return tableView.dequeueReusableCell(withIdentifier: "FlashCardSelectionTableCell", for: indexPath)
        } else if (indexPath.row == kFlashcardSortingRow) {
            return tableView.dequeueReusableCell(withIdentifier: "FlashCardSortingTableCell", for: indexPath)
        } else if (indexPath.row == kReviewOnlyModeRow) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewOnlyTableCell", for: indexPath) as! ReviewOnlyModeTableViewCell;
            cell.syncSwitchState();
            return cell;
        } else if (indexPath.row == kAutoplayRow) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AutoplayIntervalTableCell", for: indexPath)
            cell.detailTextLabel?.text = String(format: "%i Seconds", appDelegate.autoPlayInterval);
            return cell;
        } else if (indexPath.row == kDoubleClickRow) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoubleClickTableCell", for: indexPath) as! DoubleClickModeTableViewCell;
            cell.syncSwitchState();
            return cell;
        } else if (indexPath.row == kResetDefaultsRow) {
            return tableView.dequeueReusableCell(withIdentifier: "ResetDefaultsTableCell", for: indexPath)
        } else {
            return UITableViewCell(style: .default, reuseIdentifier: "Default");
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        if(indexPath.row == kReviewOnlyModeRow){
            let cell = tableView.cellForRow(at: indexPath) as! ReviewOnlyModeTableViewCell;
            cell.reviewOnlySwitch.setOn(!cell.reviewOnlySwitch.isOn, animated: true);
            tableView.deselectRow(at: indexPath, animated: true);
        }

        if(indexPath.row == kDoubleClickRow){
            let cell = tableView.cellForRow(at: indexPath) as! DoubleClickModeTableViewCell;
            cell.doubleClickSwitch.setOn(!cell.doubleClickSwitch.isOn, animated: true);
            tableView.deselectRow(at: indexPath, animated: true);
        }

        if(indexPath.row == kFlashcardSortingRow){
            let ac = UIAlertController(title: "Sorting Options", message: "Arrange the cards in the deck?", preferredStyle: UIAlertController.Style.actionSheet);

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in }
            let alphaAction = UIAlertAction(title: "Alphabetical", style: .default) { (alert: UIAlertAction!) in
                self.appDelegate.alphabeticalSort();
                self.tableView.reloadData();
            }
            let shuffleAction = UIAlertAction(title: "Shuffle", style: .default) { (alert: UIAlertAction!) in
                self.appDelegate.shuffleDeck();
                self.tableView.reloadData();
            }
            let numericalAction = UIAlertAction(title: "Numerical", style: .default) { (alert: UIAlertAction!) in
                self.appDelegate.numericalSort();
                self.tableView.reloadData();
            }

            ac.addAction(alphaAction);
            ac.addAction(numericalAction);
            ac.addAction(shuffleAction);
            ac.addAction(cancelAction);

            if let popoverController = ac.popoverPresentationController {

                let lastCell = tableView.visibleCells[indexPath.row];
                popoverController.sourceView = lastCell;
                popoverController.sourceRect = lastCell.bounds;
            }

            present(ac, animated: true, completion: nil);
        }

        if(indexPath.row == kAutoplayRow){
            let ac = UIAlertController(title: "AutoPlay Interval", message: "AutoPlay interval selection", preferredStyle: UIAlertController.Style.actionSheet);

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in }

            for interval in autoplayIntervals {

                let action = UIAlertAction(title: "\(interval) Seconds", style: .default) { (alert: UIAlertAction!) in
                    self.setAutoPlayInterval(interval);
                }
                ac.addAction(action);
            }

            ac.addAction(cancelAction);

            if let popoverController = ac.popoverPresentationController {

                let lastCell = tableView.visibleCells[indexPath.row];
                popoverController.sourceView = lastCell;
                popoverController.sourceRect = lastCell.bounds;
            }

            present(ac, animated: true, completion: nil);
        }


        //Reset to factory defaults
        if(indexPath.row == kResetDefaultsRow){
            let alertController = UIAlertController(title: "Are you sure?", message: "This will reset all of the application defaults. All settings, the Review Set, and the current deck will be reset.", preferredStyle: UIAlertController.Style.actionSheet);

            let defaultAction = UIAlertAction(title: "No Thanks.", style: UIAlertAction.Style.default, handler: nil)
            let destructiveAction = UIAlertAction(title: "Yes. Reset all the settings.", style: UIAlertAction.Style.destructive,
                handler: {(t :UIAlertAction!) in
                    let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate

                    appDelegate.resetDefaults();
                    tableView.reloadData();
            });

            alertController.addAction(defaultAction)
            alertController.addAction(destructiveAction);

            if let popoverController = alertController.popoverPresentationController {

                let lastCell = tableView.visibleCells[indexPath.row];
                popoverController.sourceView = lastCell;
                popoverController.sourceRect = lastCell.bounds;
            }

            present(alertController, animated: true, completion: nil)

            tableView.deselectRow(at: indexPath, animated: true);
        }
    }

    fileprivate func setAutoPlayInterval(_ interval :Int){
        appDelegate.autoPlayInterval = interval;
        tableView.reloadData();
    }
    
}
