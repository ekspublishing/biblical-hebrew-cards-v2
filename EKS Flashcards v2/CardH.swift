//
//  CardTypeD.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 1/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardH :Card {

    var mainHebrew :String;

    var topHebrew :String;
    var middleHebrew1 :String;
    var middleHebrew2 :String;
    var bottomHebrew :String;

    var topEnglish :String;
    var middleEnglish1 :String;
    var middleEnglish2 :String;
    var bottomEnglish :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, mainHebrew :String, topHebrew :String, middleHebrew1 :String, middleHebrew2 :String, bottomHebrew :String, topEnglish :String, middleEnglish1 :String, middleEnglish2 :String, bottomEnglish :String){

        self.mainHebrew = mainHebrew

        self.topHebrew = topHebrew
        self.middleHebrew1 = middleHebrew1
        self.middleHebrew2 = middleHebrew2
        self.bottomHebrew = bottomHebrew

        self.topEnglish = topEnglish
        self.middleEnglish1 = middleEnglish1
        self.middleEnglish2 = middleEnglish2
        self.bottomEnglish = bottomEnglish

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }

    required init(coder aDecoder: NSCoder) {

        self.mainHebrew = aDecoder.decodeObject(forKey: "mainHebrew") as! String;

        self.topHebrew = aDecoder.decodeObject(forKey: "topHebrew") as! String;
        self.middleHebrew1 = aDecoder.decodeObject(forKey: "middleHebrew1") as! String;
        self.middleHebrew2 = aDecoder.decodeObject(forKey: "middleHebrew2") as! String;
        self.bottomHebrew = aDecoder.decodeObject(forKey: "bottomHebrew") as! String;


        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.middleEnglish1 = aDecoder.decodeObject(forKey: "middleEnglish1") as! String;
        self.middleEnglish2 = aDecoder.decodeObject(forKey: "middleEnglish2") as! String;
        self.bottomEnglish = aDecoder.decodeObject(forKey: "bottomEnglish") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(mainHebrew, forKey: "mainHebrew");

        aCoder.encode(topHebrew, forKey: "topHebrew");
        aCoder.encode(middleHebrew1, forKey: "middleHebrew1");
        aCoder.encode(middleHebrew2, forKey: "middleHebrew2");
        aCoder.encode(bottomHebrew, forKey: "bottomHebrew");

        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(middleEnglish1, forKey: "middleEnglish1");
        aCoder.encode(middleEnglish2, forKey: "middleEnglish2");
        aCoder.encode(bottomEnglish, forKey: "bottomEnglish");

        super.encode(with: aCoder);
    }
}
