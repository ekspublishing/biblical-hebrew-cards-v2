//
//  DeckViewTableViewController.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/9/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class DeckViewTableViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableView.automaticDimension;

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (appDelegate.deck?.count)!;
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deckViewTableCell", for: indexPath) as! DeckViewTableViewCell;

        let card = appDelegate.deck![indexPath.row]
        cell.setCard(card);
        cell.cardView.isUserInteractionEnabled = false;
        
        if (indexPath.row == appDelegate.currentIndex && card.isEnabled){
            cell.setCurrentIndex(true, animated: false);
        } else {
            cell.setCurrentIndex(false, animated: false);
        }
        
        if (appDelegate.catalog!.revewChapter.contains(card)){
            cell.inReviewLabel.isHidden = false;
        } else {
            cell.inReviewLabel.isHidden = true;
        }
        
        if (appDelegate.isReviewMode){
            cell.enabledSwitch.isHidden = true;
        } else {
            cell.enabledSwitch.isOn = card.isEnabled
            cell.setDisabled(!card.isEnabled, animated: false);
            cell.enabledSwitch.isHidden = false;
        }

        cell.enabledSwitch.addTarget(self, action: #selector(DeckViewTableViewController.ensureCurrentIndexIsEnabled), for: .valueChanged);
        
        return cell
    }

    @objc func ensureCurrentIndexIsEnabled() {

        let deck = appDelegate.deck!;
        if (!deck[appDelegate.currentIndex].isEnabled) {

            let oldCurrentIndex = appDelegate.currentIndex;

            var newCurrentIndex = appDelegate.currentIndex;

            //try going forwards
            for i in appDelegate.currentIndex ... deck.count-1{
                if (deck[i].isEnabled){
                    newCurrentIndex = i;
                    break;
                }
            }

            //try backwards if you havent found something
            if (newCurrentIndex == oldCurrentIndex) {
                for i in (0 ... appDelegate.currentIndex ).reversed() {
                    if (deck[i].isEnabled){
                        newCurrentIndex = i;
                        break;
                    }
                }
            }

            let visibleRows = self.tableView.indexPathsForVisibleRows
            let oldCellIndexPath = IndexPath(row: oldCurrentIndex, section: 0);
            let newCellIndexPath = IndexPath(row: newCurrentIndex, section: 0);

            if (newCurrentIndex == oldCurrentIndex){

                //The user tried to disable all cards. Stop them.
                let oldCellIndexPath = IndexPath(row: oldCurrentIndex, section: 0);
                if (visibleRows!.contains(oldCellIndexPath)){
                    let cell = self.tableView.cellForRow(at: oldCellIndexPath) as! DeckViewTableViewCell;
                    cell.enabledSwitch.setOn(true, animated: true);
                    cell.setDisabled(false, animated: true);
                    cell.backingCard!.isEnabled = true;
                }

                let ac = UIAlertController(title: "All Cards Disabled", message: "You cannot disable all cards in the current deck.", preferredStyle: UIAlertController.Style.alert);
                let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in }
                ac.addAction(okAction);

                present(ac, animated: true, completion: nil);

            } else {

                if (visibleRows!.contains(oldCellIndexPath)){
                    let cell = self.tableView.cellForRow(at: oldCellIndexPath) as! DeckViewTableViewCell;
                    cell.setCurrentIndex(false, animated: true);
                }

                if (visibleRows!.contains(newCellIndexPath)){
                    let cell = self.tableView.cellForRow(at: newCellIndexPath) as! DeckViewTableViewCell;
                    cell.setCurrentIndex(true, animated: true);
                }

                appDelegate.currentIndex = newCurrentIndex;
            }

        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (appDelegate.deck![indexPath.row].isEnabled){
            
            let ac = UIAlertController(title: "Go to Card View?", message: "Do you want to jump to this card in the card view?", preferredStyle: UIAlertController.Style.alert);
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) -> Void in tableView.deselectRow(at: indexPath, animated: true)}
            
            let okAction = UIAlertAction(title: "Accept", style: .default) { (alert: UIAlertAction!) -> Void in
                self.appDelegate.currentIndex = indexPath.row;
                self.performSegue(withIdentifier: "showCardViewer", sender: nil);
            }
            
            ac.addAction(cancelAction);
            ac.addAction(okAction);
            present(ac, animated: true, completion: nil);
        } else {
        
            tableView.deselectRow(at: indexPath, animated: true);
        
        }
        
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
            appDelegate.deck?.remove(at: indexPath.row);
            
            var cell = tableView.cellForRow(at: indexPath) as! DeckViewTableViewCell;

            cell.backingCard?.isEnabled = false;

            let cellWasChecked = (cell.currentIndexIndicator.isHidden == false);
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade);
            
            
            if (tableView.cellForRow(at: indexPath) != nil) {
                cell = tableView.cellForRow(at: indexPath) as! DeckViewTableViewCell;
                if (cellWasChecked) {
                    appDelegate.currentIndex = indexPath.row;
                    cell.currentIndexIndicator.isHidden = false;
                }
            }
            
            updateTitle();
        
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        var deck = appDelegate.deck;
        
        let card = deck![sourceIndexPath.row];
        deck?.remove(at: sourceIndexPath.row);
        deck?.insert(card, at: destinationIndexPath.row);
        
        if (appDelegate.currentIndex == sourceIndexPath.row){
            appDelegate.currentIndex = destinationIndexPath.row
        }
        
        appDelegate.deck = deck;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        appDelegate.buildDeckIfRequired();
        self.tableView.reloadData();
        self.navigationController?.setToolbarHidden(false, animated: animated);
        updateTitle();
        
        if (appDelegate.deck?.count != 0 && appDelegate.currentIndex != 0){
            
            tableView.scrollToRow( at: IndexPath(row: appDelegate.currentIndex, section: 0), at: UITableView.ScrollPosition.middle, animated: false);
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (appDelegate.deck?.count == 0){
            
            appDelegate.currentIndex = 0;
            let ac = UIAlertController(title: "No Cards In Deck", message: "There are no cards in the current deck. Please enable additional cards or chapters, or ensure that Review Only Mode is disabled", preferredStyle: UIAlertController.Style.alert);
            let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) in
                self.performSegue(withIdentifier: "settingsSegue", sender: nil);
                
            }
            ac.addAction(okAction);
            present(ac, animated: true, completion: nil);
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setToolbarHidden(true, animated: animated);
        
        self.dismiss(animated: true, completion: nil);
        
    }
    
    @IBAction func sortMenuPressed(_ sender: UIBarButtonItem) {
    
        let ac = UIAlertController(title: "Sorting Options", message: "Arrange the cards in the deck?", preferredStyle: UIAlertController.Style.actionSheet);
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in }
        let alphaAction = UIAlertAction(title: "Alphabetical", style: .default) { (alert: UIAlertAction!) in
            self.appDelegate.alphabeticalSort();
            self.tableView.reloadData();
        }
        let shuffleAction = UIAlertAction(title: "Shuffle", style: .default) { (alert: UIAlertAction!) in
            self.appDelegate.shuffleDeck();
            self.tableView.reloadData();
        }
        let numericalAction = UIAlertAction(title: "Numerical", style: .default) { (alert: UIAlertAction!) in
            self.appDelegate.numericalSort();
            self.tableView.reloadData();
        }
        
        ac.addAction(alphaAction);
        ac.addAction(numericalAction);
        ac.addAction(shuffleAction);
        ac.addAction(cancelAction);
        
        if let popoverController = ac.popoverPresentationController {
            
            popoverController.barButtonItem = sender;
        }
        
        present(ac, animated: true, completion: nil);
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    
    @IBAction func flipMenuPressed(_ sender: UIBarButtonItem) {
        
        let ac = UIAlertController(title: "Flip Cards", message: "Reveal the cards in the deck?", preferredStyle: UIAlertController.Style.actionSheet);
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in }
        let frontAction = UIAlertAction(title: "Card Fronts", style: .default) { (alert: UIAlertAction!) in
            
            self.flipDeck(false)
        
        }
        let backAction = UIAlertAction(title: "Card Backs", style: .default) { (alert: UIAlertAction!) in
            self.flipDeck(true);
        }

        ac.addAction(frontAction);
        ac.addAction(backAction);
        ac.addAction(cancelAction);
        
        if let popoverController = ac.popoverPresentationController {
            
            popoverController.barButtonItem = sender;
        }
        
        present(ac, animated: true, completion: nil);
    }
    
    fileprivate func flipDeck(_ isFlipped :Bool){
    
        for cell in tableView.visibleCells{
            
            let cardView = cell.viewWithTag(7) as! CardView;
            if (cardView.backingCard!.isFlipped != isFlipped){
                cardView.flip();
            }
        }
        
        let deck = self.appDelegate.deck;
        for card in deck! {
            card.isFlipped = isFlipped;
        }
    }
    
    func updateTitle () {
        let count = appDelegate.deck?.count;
        if (appDelegate.isReviewMode){
            self.title = "Review Mode - \(count!) Cards";
        } else {
            self.title = "Current Deck - \(count!) Cards";
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
