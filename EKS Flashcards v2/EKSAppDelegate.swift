//
//  AppDelegate.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

@UIApplicationMain
class EKSAppDelegate: UIResponder, UIApplicationDelegate {

    var currentIndex = 0;
    var dirtyDeck = true;
    var deck :Array<Card>?
    var isReviewMode = false;
    var isDoubleClick = false;
    var autoPlayInterval = 5;
    var catalog :Catalog?


    var window: UIWindow?

    func resetDefaults(){

        if (FileManager.default.fileExists(atPath: dataFilePath())){
            try! FileManager.default.removeItem(atPath: dataFilePath());

        }

        deck?.removeAll();
        dirtyDeck = true;
        isReviewMode = false;
        isDoubleClick = false;
        autoPlayInterval = 5;
        currentIndex = 0;


        loadCatlog();
        buildDeckIfRequired();

        let alertController = UIAlertController(title: "Defaults have been reset", message: "The application defaults have been successfully restored.", preferredStyle: UIAlertController.Style.alert);
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil);
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Configure tracker from GoogleService-Info.plist.
        //var configureError:NSError?
        //GGLContext.sharedInstance().configureWithError(&configureError)
        //assert(configureError == nil, "Error configuring Google services: \(configureError)")

        // Optional: configure GAI options.
        //let gai = GAI.sharedInstance()
        //gai?.trackUncaughtExceptions = true  // report uncaught exceptions
        //gai.logger.logLevel = GAILogLevel.Verbose  // remove before app release


        application.isIdleTimerDisabled = true;

        //Check for an application state archive
        let archiveURL = dataFilePath();

        if (FileManager.default.fileExists(atPath: archiveURL)){
            let data = NSMutableData(contentsOfFile: dataFilePath());
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data! as Data);

            currentIndex = unarchiver.decodeInteger(forKey: "currentIndex");
            dirtyDeck = unarchiver.decodeBool(forKey: "dirtyDeck");
            autoPlayInterval = unarchiver.decodeInteger(forKey: "autoPlayInterval")
            isReviewMode = unarchiver.decodeBool(forKey: "isReviewMode")
            isDoubleClick = unarchiver.decodeBool(forKey: "isDoubleClick");
            deck = unarchiver.decodeObject(forKey: "deck") as? [Card]
            catalog = unarchiver.decodeObject(forKey: "catalog") as? Catalog;
        } else {
            self.loadCatlog();

            //Delete any old archive files from previous versions of the app
            let docsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask);
            let docDirectory = docsDirectories.first!;

            let dirPath = docDirectory.path;
            let contents = try! FileManager.default.contentsOfDirectory(atPath: dirPath);
            for directoryContent in contents {

                let path = dirPath + "/" + directoryContent
                print("Deleting file \(path)");
                try! FileManager.default.removeItem(atPath: path);
            }

        }

        self.buildDeckIfRequired();

        Thread.sleep(forTimeInterval: 1);

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        saveApplicationState();
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func shuffleDeck() {

        deck?.shuffle();

        let alertController = UIAlertController(title: "Deck Successfully Arranged", message: "The deck was shuffled", preferredStyle: UIAlertController.Style.alert);
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil);
    }

    func numericalSort() {

        deck = deck?.sorted(by: {$0.numericalIndex < $1.numericalIndex});

        let alertController = UIAlertController(title: "Deck Successfully Arranged", message: "The deck was numerically ordered", preferredStyle: UIAlertController.Style.alert);
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil);
    }

    func alphabeticalSort() {
        deck = deck?.sorted(by: {$0.alphabeticalIndex < $1.alphabeticalIndex});

        let alertController = UIAlertController(title: "Deck Successfully Arranged", message: "The deck was alphabetically ordered", preferredStyle: UIAlertController.Style.alert);
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil);
    }


    fileprivate func loadCatlog() {

        self.catalog = Catalog.loadCatalog();
    }

    fileprivate func saveApplicationState() {

        let data = NSMutableData();
        let archiver = NSKeyedArchiver(forWritingWith: data);
        let path = dataFilePath();

        archiver.encode(isReviewMode, forKey: "isReviewMode");
        archiver.encode(isDoubleClick, forKey: "isDoubleClick");
        archiver.encode(autoPlayInterval, forKey: "autoPlayInterval");


        archiver.encode(currentIndex, forKey: "currentIndex");
        archiver.encode(dirtyDeck, forKey: "dirtyDeck");

        archiver.encode(deck, forKey: "deck")
        archiver.encode(catalog, forKey: "catalog")

        archiver.finishEncoding();
        data.write(toFile: path, atomically: true)
    
    }

    fileprivate func dataFilePath() -> String {

        let docsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask);
        let docDirectory = docsDirectories.first!;

        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let build = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as! String
        let bundle = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as! String;

        let archiveName = "\(bundle)-\(version)-\(build)";
        return docDirectory.appendingPathComponent(archiveName).path
    }

    func buildDeckIfRequired() {

        if(deck != nil && !self.dirtyDeck) { return }

        currentIndex = 0;

        if (deck == nil){
            deck = [Card]();
        } else {
            deck?.removeAll();
        }

        if let theCatalog = catalog {

            let reviewChapter = theCatalog.revewChapter;

            if (reviewChapter.isEnabled || self.isReviewMode){

                for card in reviewChapter.cards{

                    //Ignore card level enabled or disabled on review items
                    card.isFlipped = false;
                    if (!deck!.contains(card)){
                        deck?.append(card);
                    }

                }
            }

            if(!self.isReviewMode){

                for chapter in theCatalog.chapters {
                    if (chapter.isEnabled){
                        for card in chapter.cards{
                            card.isFlipped = false;
                            if (!deck!.contains(card)){
                                deck?.append(card);
                            }

                        }
                    }
                }
            }
        }
        
        self.dirtyDeck = false;
    }
    
}

