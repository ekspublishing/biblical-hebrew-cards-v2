//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 3/4/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardR5 :Card {

    var backHebrew :String;
    var topEnglish :String
    var bottomEnglish :String
    var topItalics :String;
    var middleItalics :String
    var bottomItalics :String

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, backHebrew :String, topEnglish :String, bottomEnglish :String, topItalics :String, middleItalics :String, bottomItalics :String, redCoding :String) {
        self.topEnglish = topEnglish;
        self.bottomEnglish = bottomEnglish;
        self.backHebrew = backHebrew
        self.topItalics = topItalics;
        self.middleItalics = middleItalics
        self.bottomItalics = bottomItalics

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: redCoding);

    }

    required init(coder aDecoder: NSCoder) {


        self.backHebrew = aDecoder.decodeObject(forKey: "backHebrew") as! String;
        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.bottomEnglish = aDecoder.decodeObject(forKey: "bottomEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        self.middleItalics = aDecoder.decodeObject(forKey: "middleItalics") as! String;
        self.bottomItalics = aDecoder.decodeObject(forKey: "bottomItalics") as! String;

        super.init(coder: aDecoder);
    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(backHebrew, forKey: "backHebrew");
        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(bottomEnglish, forKey: "bottomEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        aCoder.encode(middleItalics, forKey: "middleItalics");
        aCoder.encode(bottomItalics, forKey: "bottomItalics")

        super.encode(with: aCoder);
    }
    
}
