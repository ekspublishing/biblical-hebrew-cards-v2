//
//  CatalogLoader.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/12/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

open class CatalogLoader: NSObject, CHCSVParserDelegate {
    
    var lineIndex = 0;
    var rowIndex = 0;
    var header = [String]();
    var currentLine = [String]();
    
    var cards: [Dictionary<String, String>] = [];
    
    
    
    @objc open func parser(_ parser: CHCSVParser!, didFailWithError error: NSError!) {
        print(error);
    }
    
    @objc open func parserDidBeginDocument(_ parser: CHCSVParser!) {
        lineIndex = 0;
        rowIndex = 0;
    }
    
    @objc open func parserDidEndDocument(_ parser: CHCSVParser!) {
    }
    
    @objc open func parser(_ parser: CHCSVParser!, didBeginLine recordNumber: UInt) {
        currentLine.removeAll();
    }
    
    @objc open func parser(_ parser: CHCSVParser!, didEndLine recordNumber: UInt) {
        
        if (rowIndex == 0){
            header.append(contentsOf: currentLine);
        } else {
            
            var cardDictionary: [String:String] = [:];
            
            for i in 0...currentLine.count-1{

                let headerString = header[i];
                let currLineText = currentLine[i]
                cardDictionary[headerString] =  currLineText;
            }
            
            cards.append(cardDictionary);
        }
        
        rowIndex += 1;
        
    }
    
    @objc open func parser(_ parser: CHCSVParser!, didReadField field: String!, at fieldIndex: Int) {
        
        //Strip \" charachter
        let strippedField = field.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        //Trim any whitespace hanging out
        let trimmedString = strippedField.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        
        currentLine.append(trimmedString)
        //print(field);
    }
    
}
