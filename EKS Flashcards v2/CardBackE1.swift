//
//  CardBackE.swift
//  EKS Flashcards
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackE1: CardFace {

    let nibName = "CardBackE1"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var topEnglish: UILabel!

    @IBOutlet var topLeftHebrewLabel: UILabel!
    @IBOutlet var bottomLeftHebrewLabel: UILabel!

    @IBOutlet var topRightHebrewLabel: UILabel!
    @IBOutlet var bottomRightHebrewLabel: UILabel!


    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        topLeftHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);
        bottomLeftHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);
        topRightHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);
        bottomRightHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16.8;


        topLeftHebrewLabel.font = topLeftHebrewLabel.font.withSize(fontSize * 2);
        bottomLeftHebrewLabel.font = bottomLeftHebrewLabel.font.withSize(fontSize * 2)
        topRightHebrewLabel.font = topRightHebrewLabel.font.withSize(fontSize * 2);
        bottomRightHebrewLabel.font = bottomRightHebrewLabel.font.withSize(fontSize * 2);

        topEnglish.font = topEnglish.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        applyFormatting();

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);
        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

    }

    override func setCard(_ card :Card){

        if let card_E1 = card as? CardE1 {

            backingCard = card_E1;

            chapterDescriptionLabel.text = card_E1.chapterDescription;

            self.topEnglish.text = card_E1.topEnglish

            self.topLeftHebrewLabel.text = card_E1.leftTopHebrew
            self.bottomLeftHebrewLabel.text = card_E1.leftBottomHebrew

            self.topRightHebrewLabel.text = card_E1.rightTopHebrew
            self.bottomRightHebrewLabel.text = card_E1.rightBottomHebrew

            numericalIndexLabel.text = String(card_E1.numericalIndex);


            applyFormatting();
        }

    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let e1Card = backingCard as! CardE1

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()
            
            //the trailing space is to handle a clipping issue.
            var topLeftCombo = e1Card.leftTopItalics + " " + e1Card.leftTopHebrew + " " as NSString;
            var bottomLeftCombo = e1Card.leftBottomItalics + " " + e1Card.leftBottomHebrew + " " as NSString;
            var topRightCombo = e1Card.rightTopItalics + " " + e1Card.rightTopHebrew + " " as NSString;
            var bottomRightCombo = e1Card.rightBottomItalics + " " + e1Card.rightBottomHebrew + " " as NSString;

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"
            topLeftCombo = topLeftCombo.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString
            bottomLeftCombo = bottomLeftCombo.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString
            topRightCombo = topRightCombo.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString
            bottomRightCombo = bottomRightCombo.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString

            let combinedString1 = NSMutableAttributedString(string: topLeftCombo as String);
            if (e1Card.leftTopItalics != ""){
                let range = topLeftCombo.range(of: e1Card.leftTopItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topLeftHebrewLabel.font.pointSize/2)
                combinedString1.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            topLeftHebrewLabel.attributedText = combinedString1;

            let combinedString2 = NSMutableAttributedString(string: bottomLeftCombo as String);
            if (e1Card.leftBottomItalics != ""){
                let range = bottomLeftCombo.range(of: e1Card.leftBottomItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: bottomLeftHebrewLabel.font.pointSize/2)
                combinedString2.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            bottomLeftHebrewLabel.attributedText = combinedString2;

            let combinedString3 = NSMutableAttributedString(string: topRightCombo as String);
            if (e1Card.rightTopItalics != ""){
                let range = topRightCombo.range(of: e1Card.rightTopItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topRightHebrewLabel.font.pointSize/2)
                combinedString3.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            topRightHebrewLabel.attributedText = combinedString3;

            let combinedString4 = NSMutableAttributedString(string: bottomRightCombo as String);
            if (e1Card.rightBottomItalics != ""){
                let range = bottomRightCombo.range(of: e1Card.rightBottomItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: bottomRightHebrewLabel.font.pointSize/2)
                combinedString4.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            bottomRightHebrewLabel.attributedText = combinedString4;
        }
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let e1Card = backingCard as! CardE1

            let lTHebrew = e1Card.leftTopHebrew as NSString
            let lBHebrew = e1Card.leftBottomHebrew as NSString
            let rTHebrew = e1Card.rightTopHebrew as NSString
            let rBHebrew = e1Card.rightBottomHebrew as NSString

            let formattedTopLeftHebrew = NSMutableAttributedString(string: lTHebrew as String)
            let formattedBottomLeftHebrew = NSMutableAttributedString(string: lBHebrew as String)
            let formattedTopRightHebrew = NSMutableAttributedString(string: rTHebrew as String)
            let formattedBottomRightHebrew = NSMutableAttributedString(string: rBHebrew as String)

            if (formattedTopLeftHebrew.length != 0){
                let range = lTHebrew.range(of: "־")
                formattedTopLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(topLeftHebrewLabel.font.pointSize/14), range: range)
            }
            topLeftHebrewLabel.attributedText = formattedTopLeftHebrew

            if (formattedBottomLeftHebrew.length != 0){
                let range = lBHebrew.range(of: "־")
                formattedBottomLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomLeftHebrewLabel.font.pointSize/14), range: range)
            }
            bottomLeftHebrewLabel.attributedText = formattedBottomLeftHebrew

            if (formattedTopRightHebrew.length != 0){
                let range = rTHebrew.range(of: "־")
                formattedTopRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(topRightHebrewLabel.font.pointSize/14), range: range)
            }
            topRightHebrewLabel.attributedText = formattedTopRightHebrew

            if (formattedBottomRightHebrew.length != 0){
                let range  = rBHebrew.range(of: "־")
                formattedBottomRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomRightHebrewLabel.font.pointSize/14), range: range)
            }
            bottomRightHebrewLabel.attributedText = formattedBottomRightHebrew

        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.e1

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
