//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 2/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackD: CardFace {

    let nibName = "CardBackD"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var topEnglish: UILabel!

    @IBOutlet var bottomEnglish: UILabel!

    @IBOutlet var leftTopHebrew: UILabel!
    @IBOutlet var leftBottomHebrew: UILabel!

    @IBOutlet var rightTopHebrew: UILabel!
    @IBOutlet var rightBottomHebrew: UILabel!


    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        leftTopHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        leftBottomHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        rightTopHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        rightBottomHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;


        leftTopHebrew.font = leftTopHebrew.font.withSize(fontSize * 2);
        leftBottomHebrew.font = leftBottomHebrew.font.withSize(fontSize * 2)
        rightTopHebrew.font = rightTopHebrew.font.withSize(fontSize * 2);
        rightBottomHebrew.font = rightBottomHebrew.font.withSize(fontSize * 2);


        bottomEnglish.font = bottomEnglish.font.withSize(fontSize);
        topEnglish.font = topEnglish.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        applyFormatting();

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);
        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

    }

    override func setCard(_ card :Card){

        if let card_D = card as? CardD {

            backingCard = card_D;

            chapterDescriptionLabel.text = card_D.chapterDescription;

            self.topEnglish.text = card_D.topEnglish
            self.bottomEnglish.text = card_D.bottomEnglish

            self.leftTopHebrew.text = card_D.leftTopHebrew
            self.leftBottomHebrew.text = card_D.leftBottomHebrew

            self.rightTopHebrew.text = card_D.rightTopHebrew
            self.rightBottomHebrew.text = card_D.rightBottomHebrew

            numericalIndexLabel.text = String(card_D.numericalIndex);

            applyFormatting();
        }

    }


    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let dCard = backingCard as! CardD

            //format the maqaf in the hebrew if there is one
            hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            var combo = dCard.topEnglish + " " + dCard.topItalics + " " as NSString;

            var combinedString = NSMutableAttributedString(string: combo as String);
            if (dCard.topItalics.count != 0){
                let range = combo.range(of: dCard.topItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglish.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            topEnglish.attributedText = combinedString;

            //the trailing space is to handle a clipping issue.
            combo = dCard.bottomEnglish + " " + dCard.bottomItalics + " " as NSString;

            combinedString = NSMutableAttributedString(string: combo as String);
            if (dCard.bottomItalics.count != 0){
                let range = combo.range(of: dCard.bottomItalics);
                combinedString.addAttribute(NSAttributedString.Key.font, value: UIFont.italicSystemFont(ofSize: bottomEnglish.font.pointSize/2), range: range)
            }

            bottomEnglish.attributedText = combinedString;
        }
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let dCard = backingCard as! CardD

            let lTHebrew = dCard.leftTopHebrew as NSString
            let lBHebrew = dCard.leftBottomHebrew as NSString
            let rTHebrew = dCard.rightTopHebrew as NSString
            let rBHebrew = dCard.rightBottomHebrew as NSString

            let formattedTopLeftHebrew = NSMutableAttributedString(string: lTHebrew as String)
            let formattedBottomLeftHebrew = NSMutableAttributedString(string: lBHebrew as String)
            let formattedTopRightHebrew = NSMutableAttributedString(string: rTHebrew as String)
            let formattedBottomRightHebrew = NSMutableAttributedString(string: rBHebrew as String)

            if (formattedTopLeftHebrew.length != 0){
                let range = lTHebrew.range(of: "־")
                formattedTopLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftTopHebrew.font.pointSize/14), range: range)
            }
            leftTopHebrew.attributedText = formattedTopLeftHebrew

            if (formattedBottomLeftHebrew.length != 0){
                let range = lBHebrew.range(of: "־")
                formattedBottomLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftBottomHebrew.font.pointSize/14), range: range)
            }
            leftBottomHebrew.attributedText = formattedBottomLeftHebrew

            if (formattedTopRightHebrew.length != 0){
                let range = rTHebrew.range(of: "־")
                formattedTopRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightTopHebrew.font.pointSize/14), range: range)
            }
            rightTopHebrew.attributedText = formattedTopRightHebrew

            if (formattedBottomRightHebrew.length != 0){
                let range  = rBHebrew.range(of: "־")
                formattedBottomRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightBottomHebrew.font.pointSize/14), range: range)
            }
            rightBottomHebrew.attributedText = formattedBottomRightHebrew

        }
    }


    func xibSetup() {

        //Set the card type
        cardType = CardType.d

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
