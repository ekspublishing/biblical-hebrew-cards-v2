//
//  CardSelectionTableViewController.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardSelectionTableViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate

    var backingChapter :Chapter?
    @IBOutlet var editButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        if (!backingChapter!.isReview){
            editButton.isEnabled = false;
            editButton.tintColor = UIColor.clear;
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (backingChapter?.count())!;
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CardSelectionCell", for: indexPath) as! CardSelectionTableViewCell;

        let card = appDelegate.deck![indexPath.row]
        cell.setCard((backingChapter?.cardAt(indexPath.row))!)

        cell.cardEnabledSwitch.isHidden = backingChapter!.isReview

        if (appDelegate.catalog!.revewChapter.contains(card)){
            cell.inReviewLabel.isHidden = false;
        } else {
            cell.inReviewLabel.isHidden = true;
        }

        cell.setDisabled(!cell.backingCard!.isEnabled, animated: false);
        
        return cell
    }


    func setChapter(_ chapter :Chapter){
        self.backingChapter = chapter
        self.title = chapter.title;
    }

    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return backingChapter!.isReview;
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return backingChapter!.isReview;
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {

            backingChapter!.cards.remove(at: indexPath.row);
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade);

        }
    }

    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

        let card = backingChapter?.cards[sourceIndexPath.row];
        backingChapter?.cards.remove(at: sourceIndexPath.row);
        backingChapter?.cards.insert(card!, at: destinationIndexPath.row);
    }

    @IBAction func toggleEdit(_ sender: AnyObject) {

        self.tableView.setEditing(!tableView.isEditing, animated: true);

    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
}
