//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackB: CardFace {

    let nibName = "CardBackB"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var leftItalicsLabel: UILabel!
    @IBOutlet weak var leftHebrewLabel: UILabel!
    @IBOutlet weak var leftEnglishLabel: UILabel!

    @IBOutlet weak var rightItalicsLabel: UILabel!
    @IBOutlet weak var rightHebrewLabel: UILabel!
    @IBOutlet weak var rightEnglishLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();


        leftHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);
        rightHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);


        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;

        leftHebrewLabel.font = leftHebrewLabel.font.withSize(fontSize * 2);
        leftEnglishLabel.font = leftEnglishLabel.font.withSize(fontSize);


        rightHebrewLabel.font = rightHebrewLabel.font.withSize(fontSize * 2);
        rightEnglishLabel.font = rightEnglishLabel.font.withSize(fontSize);


        fontSize = CGFloat((rootView?.frame.size.width)!) / 20;
        rightItalicsLabel.font = rightItalicsLabel.font.withSize(fontSize);
        leftItalicsLabel.font = leftItalicsLabel.font.withSize(fontSize);


        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting()
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let bCard = backingCard as! CardB

            let lHebrew = bCard.leftHebrew as NSString
            let rHebrew = bCard.rightHebrew as NSString

            let formattedLeftHebrew = NSMutableAttributedString(string: lHebrew as String)
            let formattedRightHebrew = NSMutableAttributedString(string: rHebrew as String)

            if (formattedLeftHebrew.length != 0){
                let range = lHebrew.range(of: "־")
                formattedLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftHebrewLabel.font.pointSize/14), range: range)
            }
            leftHebrewLabel.attributedText = formattedLeftHebrew

            if (formattedRightHebrew.length != 0){
                let range = rHebrew.range(of: "־")
                formattedRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightHebrewLabel.font.pointSize/14), range: range)
            }
            rightHebrewLabel.attributedText = formattedRightHebrew
        }
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let bCard = backingCard as! CardB

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            var lEnglish = bCard.leftEnglish as NSString;
            var rEnglish = bCard.rightEnglish as NSString;

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"
            lEnglish = lEnglish.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString
            rEnglish = rEnglish.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString

            let leftEnglishString = NSMutableAttributedString(string: lEnglish as String);
            let rightEnglishString = NSMutableAttributedString(string: rEnglish as String);
            leftEnglishLabel.attributedText = leftEnglishString;
            rightEnglishLabel.attributedText = rightEnglishString;

            #if PHCARDS
                changeCommaSize(bCard.leftHebrew, targetMutableString: &leftHebrewLabel.attributedText!, size: leftHebrewLabel.font.pointSize / 1.5)
                changeCommaSize(bCard.rightHebrew, targetMutableString: &rightHebrewLabel.attributedText!, size: rightHebrewLabel.font.pointSize / 1.5)
            #endif

        }
    }

    override func setCard(_ card :Card){

        if let card_B = card as? CardB {
            chapterDescriptionLabel.text = card_B.chapterDescription;

            leftItalicsLabel.text = card_B.leftItalics;
            leftHebrewLabel.text = card_B.leftHebrew;
            leftEnglishLabel.text = card_B.leftEnglish;

            rightItalicsLabel.text = card_B.rightItalics;
            rightHebrewLabel.text = card_B.rightHebrew;
            rightEnglishLabel.text = card_B.rightEnglish;

            numericalIndexLabel.text = String(card_B.numericalIndex);

            backingCard = card;

            applyFormatting()
        }

    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.b

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
