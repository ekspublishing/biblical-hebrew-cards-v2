//
//  ReviewOnlyModeTableViewCell.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class DoubleClickModeTableViewCell: UITableViewCell {

    @IBOutlet weak var doubleClickSwitch: UISwitch!
    
    @IBAction func doubleClickToggled(_ sender: UISwitch) {
        let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate;
        appDelegate.isDoubleClick = sender.isOn;
    
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        syncSwitchState();
    }
    
    func syncSwitchState(){
        let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
        doubleClickSwitch.setOn(appDelegate.isDoubleClick, animated: false);
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //weird apple behvaior where setselected gets fired during table intialization and then the switch toggles without firing the handler above
        if(animated){
            doubleClickToggled(doubleClickSwitch)
        }
    }
}
