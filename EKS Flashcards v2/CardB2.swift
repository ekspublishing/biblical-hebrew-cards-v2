//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

class CardB2 :Card {

    var topEnglish :String

    var leftHebrew :String;
    var leftItalics :String;

    var rightHebrew :String;
    var rightItalics :String;

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, topEnglish :String, leftHebrew :String, leftItalics :String, rightHebrew :String, rightItalics :String) {

        self.topEnglish = topEnglish;

        self.leftHebrew = leftHebrew;
        self.leftItalics = leftItalics;
        self.rightHebrew = rightHebrew;
        self.rightItalics = rightItalics;

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }

    required init(coder aDecoder: NSCoder) {

        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;

        self.leftHebrew = aDecoder.decodeObject(forKey: "leftHebrew") as! String;
        self.leftItalics = aDecoder.decodeObject(forKey: "leftItalics") as! String;

        self.rightHebrew = aDecoder.decodeObject(forKey: "rightHebrew") as! String;
        self.rightItalics = aDecoder.decodeObject(forKey: "rightItalics") as! String;

        super.init(coder: aDecoder);

    }

    override func encode(with aCoder: NSCoder) {

        aCoder.encode(topEnglish, forKey: "topEnglish");

        aCoder.encode(leftHebrew, forKey: "leftHebrew");
        aCoder.encode(leftItalics, forKey: "leftItalics");

        aCoder.encode(rightHebrew, forKey: "rightHebrew");
        aCoder.encode(rightItalics, forKey: "rightItalics");

        super.encode(with: aCoder);
    }
}
