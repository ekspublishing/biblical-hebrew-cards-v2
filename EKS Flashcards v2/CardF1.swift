//
//  CardTypeD.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 1/2/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardF1 :Card {
    
    
    var topEnglish :String;
    var topItalics :String;
    
    var leftHebrew :String;
    var rightHebrew :String;
    
    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String,
        topEnglish :String, topItalics :String, leftHebrew :String, rightHebrew :String){
            
            self.topEnglish = topEnglish
            self.topItalics = topItalics
            
            self.leftHebrew = leftHebrew
            self.rightHebrew = rightHebrew
            
            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        
        self.leftHebrew = aDecoder.decodeObject(forKey: "leftHebrew") as! String;
        self.rightHebrew = aDecoder.decodeObject(forKey: "rightHebrew") as! String;
        
        super.init(coder: aDecoder);
        
    }
    
    override func encode(with aCoder: NSCoder) {
        
        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        
        aCoder.encode(leftHebrew, forKey: "leftHebrew");
        aCoder.encode(rightHebrew, forKey: "rightHebrew");
        
        super.encode(with: aCoder);
    }
}
