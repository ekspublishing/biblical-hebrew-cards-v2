//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

class CardA :Card {
    
    var backHebrew :String;
    var backEnglish :String
    var backItalics :String;
    
    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, backHebrew :String, backEnglish :String, backItalics :String) {
        self.backEnglish = backEnglish;
        self.backHebrew = backHebrew;
        self.backItalics = backItalics;
        
            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);
 
    }
    
    required init(coder aDecoder: NSCoder) {

        
        self.backHebrew = aDecoder.decodeObject(forKey: "backHebrew") as! String;
        self.backEnglish = aDecoder.decodeObject(forKey: "backEnglish") as! String;
        self.backItalics = aDecoder.decodeObject(forKey: "backItalics") as! String;
        
        super.init(coder: aDecoder);
    
    }
    
    override func encode(with aCoder: NSCoder) {
        
        aCoder.encode(backHebrew, forKey: "backHebrew");
        aCoder.encode(backEnglish, forKey: "backEnglish");
        aCoder.encode(backItalics, forKey: "backItalics");
        
        
        super.encode(with: aCoder);
        
        
    }

}
