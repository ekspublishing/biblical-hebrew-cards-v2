//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardFront: RedCardFace {

    let nibName = "CardFront"
    var backingCard :Card?
    @IBOutlet weak var frontLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Set the custom hebrew font
        let font = UIFont(name: Catalog.fontName, size: 30);

        //Adjust main label font size
        let rootView = self.view;
        let fontSize = CGFloat((rootView?.frame.size.width)!) / 3.5;
        frontLabel.font = font!.withSize(fontSize);

        applyFormatting();
    }

    override func setCard(_ card :Card){

        backingCard = card;
        frontLabel.text = card.frontHebrew;
        applyFormatting();
    }

    fileprivate func applyFormatting(){
        //hebrewMaqafFormatting();
        applyRed(backingCard!.frontHebrew, redIndexString: backingCard!.redCoding, targetMutableString: &frontLabel.attributedText!)
    }

    fileprivate func hebrewMaqafFormatting(){

        let hebrew = frontLabel.text! as NSString

        let formattedHebrew = NSMutableAttributedString(string: hebrew as String)

        if (formattedHebrew.length != 0){
            let range = hebrew.range(of: "־")
            formattedHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(frontLabel.font.pointSize/14), range: range)
        }
        frontLabel.attributedText = formattedHebrew
    }

    func xibSetup() {
        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }

    func loadViewFromNib() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
