//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackC: CardFace {

    let nibName = "CardBackC"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet var topEnglish: UILabel!
    @IBOutlet var leftHebrew: UILabel!
    @IBOutlet var rightHebrew: UILabel!
    @IBOutlet var bottomHebrew: UILabel!
    @IBOutlet var bottomEnglish: UILabel!


    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        //Adjust font sizes
        let rootView = self.view;
        leftHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        rightHebrew.font = UIFont(name: Catalog.fontName, size: 30);
        bottomHebrew.font = UIFont(name: Catalog.fontName, size: 30);

        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;

        leftHebrew.font = leftHebrew.font.withSize(fontSize * 2);
        rightHebrew.font = rightHebrew.font.withSize(fontSize * 2);
        bottomHebrew.font = bottomHebrew.font.withSize(fontSize * 2);


        bottomEnglish.font = bottomEnglish.font.withSize(fontSize);
        topEnglish.font = topEnglish.font.withSize(fontSize);


        fontSize = CGFloat((rootView?.frame.size.width)!) / 20;

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        applyFormatting();

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;
    }

    override func setCard(_ card :Card){

        if let card_C = card as? CardC {

            backingCard = card_C;

            chapterDescriptionLabel.text = card_C.chapterDescription;


            self.leftHebrew.text = card_C.leftHebrew;
            self.rightHebrew.text = card_C.rightHebrew;
            self.bottomHebrew.text = card_C.bottomHebrew;

            self.bottomEnglish.text = card_C.bottomEnglish;
            self.topEnglish.text = card_C.topEnglish;

            numericalIndexLabel.text = String(card_C.numericalIndex);

            applyFormatting();
        }

    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let cCard = backingCard as! CardC

            //format the maqaf in the hebrew if there is one
            hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            let combo = cCard.topEnglish + " " + cCard.topItalics + " " as NSString;

            let combinedString = NSMutableAttributedString(string: combo as String);
            if (cCard.topItalics != ""){
                let range = combo.range(of: cCard.topItalics);
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglish.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            topEnglish.attributedText = combinedString;
        }
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let cCard = backingCard as! CardC

            let lHebrew = cCard.leftHebrew as NSString
            let rHebrew = cCard.rightHebrew as NSString
            let bHebrew = cCard.bottomHebrew as NSString

            let formattedLeftHebrew = NSMutableAttributedString(string: lHebrew as String)
            let formattedRightHebrew = NSMutableAttributedString(string: rHebrew as String)
            let formattedBottomHebrew = NSMutableAttributedString(string: bHebrew as String)

            if (formattedLeftHebrew.length != 0){
                let range = lHebrew.range(of: "־")
                formattedLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftHebrew.font.pointSize/14), range: range)
            }
            leftHebrew.attributedText = formattedLeftHebrew

            if (formattedRightHebrew.length != 0){
                let range = rHebrew.range(of: "־")
                formattedRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightHebrew.font.pointSize/14), range: range)
            }
            rightHebrew.attributedText = formattedRightHebrew

            if (formattedBottomHebrew.length != 0){
                let range  = bHebrew.range(of: "־")
                formattedBottomHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomHebrew.font.pointSize/14), range: range)
            }
            bottomHebrew.attributedText = formattedBottomHebrew

        }
    }


    func xibSetup() {

        //Set the card type
        cardType = CardType.c

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
