//
//  CardTypeC.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

class CardC :Card {
    
    
    var leftHebrew :String;
    var rightHebrew :String;
    var bottomHebrew :String;
    
    var topEnglish :String;
    var topItalics :String;
    
    var bottomEnglish :String;
    
    
    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String,
        leftHebrew :String, rightHebrew :String, bottomHebrew :String, topEnglish :String, topItalics :String, bottomEnglish :String) {

            self.leftHebrew = leftHebrew;
            self.rightHebrew = rightHebrew;
            self.bottomHebrew = bottomHebrew;
            self.topEnglish = topEnglish
            self.topItalics = topItalics;
            self.bottomEnglish = bottomEnglish;
            
            super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: nil);

    }
    
    required init(coder aDecoder: NSCoder) {
        
        
        self.leftHebrew = aDecoder.decodeObject(forKey: "leftHebrew") as! String;
        self.rightHebrew = aDecoder.decodeObject(forKey: "rightHebrew") as! String;
        self.bottomHebrew = aDecoder.decodeObject(forKey: "bottomHebrew") as! String;
        
        self.topEnglish = aDecoder.decodeObject(forKey: "topEnglish") as! String;
        self.topItalics = aDecoder.decodeObject(forKey: "topItalics") as! String;
        self.bottomEnglish = aDecoder.decodeObject(forKey: "bottomEnglish") as! String;
        
        super.init(coder: aDecoder);
        
    }
    
    override func encode(with aCoder: NSCoder) {
        
        aCoder.encode(leftHebrew, forKey: "leftHebrew");
        aCoder.encode(rightHebrew, forKey: "rightHebrew");
        aCoder.encode(bottomHebrew, forKey: "bottomHebrew");
        
        aCoder.encode(topEnglish, forKey: "topEnglish");
        aCoder.encode(topItalics, forKey: "topItalics");
        aCoder.encode(bottomEnglish, forKey: "bottomEnglish");
    
        super.encode(with: aCoder);
    }
}
