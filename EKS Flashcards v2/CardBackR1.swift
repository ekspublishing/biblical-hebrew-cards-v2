
import UIKit

class CardBackR1: RedCardFace {

    let nibName = "CardBackR1"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var hebrewLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var italicsLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        let font = UIFont(name: Catalog.fontName, size: 30);
        hebrewLabel.font = font;

        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 12;
        englishLabel.font = englishLabel.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 20
        italicsLabel.font = italicsLabel.font.withSize(fontSize);

        //Hebrew font is weirdly small
        hebrewLabel.font = hebrewLabel.font.withSize(fontSize * 2 );

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting()
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let r1Card = backingCard as! CardR1

            let hebrew = r1Card.backHebrew as NSString

            let formattedHebrew = NSMutableAttributedString(string: hebrew as String)

            if (formattedHebrew.length != 0){
                let range = hebrew.range(of: "־")
                formattedHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(hebrewLabel.font.pointSize/14), range: range)
            }
            hebrewLabel.attributedText = formattedHebrew
        }
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let r1Card = backingCard as! CardR1

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            applyRed(r1Card.backHebrew, redIndexString: r1Card.redCoding, targetMutableString: &hebrewLabel.attributedText!)
        }
        
    }

    override func setCard(_ card :Card){

        if let card_R1 = card as? CardR1 {
            chapterDescriptionLabel.text = card_R1.chapterDescription;
            englishLabel.text = card_R1.backEnglish;
            hebrewLabel.text = card_R1.backHebrew;
            italicsLabel.text = " " + card_R1.backItalics + " ";
            numericalIndexLabel.text = String(card_R1.numericalIndex);
            backingCard = card;

            hebrewMaqafFormatting()
            applyFormatting()
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.r1

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }

    func loadViewFromNib() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
