
//
//  CardFace.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/9/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardFace: UIView {

    var cardType :CardType?;

    func setCard(_ card :Card){

    }

    //change font in Hebrew for all weird periods, commas, and elipses' in PH
    func changeCommaSize(_ hebrew :String, targetMutableString :inout NSAttributedString, size :CGFloat) {

        let commaValue = UInt32(44)

        var currentIndex = 0

        let formattedHebrew = NSMutableAttributedString(string: hebrew)

        let font = UIFont(name: "Palatino", size: size)

        for scalar in hebrew.unicodeScalars {

            if scalar.value == commaValue {
                let range = NSRange(location: currentIndex, length: 1)
                formattedHebrew.addAttribute(NSAttributedString.Key.font, value: font!, range: range)
            }

            currentIndex += 1
        }
        targetMutableString = formattedHebrew
    }
}
