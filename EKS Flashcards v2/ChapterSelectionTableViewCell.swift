//
//  ChapterSelectionTableViewCell.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class ChapterSelectionTableViewCell: UITableViewCell {
    
    var backingChapter :Chapter?;
    
    @IBOutlet weak var chapterEnabledSwitch: UISwitch!
    @IBOutlet weak var chapterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setChapter(_ chapter :Chapter){
        self.backingChapter = chapter;
        self.chapterLabel.text = chapter.title;
        self.chapterEnabledSwitch.setOn(chapter.isEnabled, animated: false);
    }
    
    @IBAction func switchValueChanged(_ sender: AnyObject) {
        
        //need to unwrap
        if let chap = backingChapter {
            chap.isEnabled = !chap.isEnabled;
            
            let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate;
            appDelegate.dirtyDeck = true;
        }

    }
    
    func setDisabled(_ isDisabled: Bool){
        chapterEnabledSwitch.isUserInteractionEnabled = !isDisabled
        chapterLabel.isEnabled = !isDisabled;
        self.isUserInteractionEnabled = !isDisabled;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
