//
//  CardTypeA.swift
//  EKS Flashcards v2
//
//  Created by Simon Budker on 3/4/16.
//  Copyright © 2016 EKS Publishing. All rights reserved.
//

import Foundation

class CardR6 :Card {

    var backHebrew :String;
    var topEnglish :String
    var bottomEnglish :String
    var topItalics :String;
    var middleItalics :String
    var bottomItalics :String

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, backHebrew :String, topEnglish :String, bottomEnglish :String, topItalics :String, middleItalics :String, bottomItalics :String, redCoding :String) {
        self.topEnglish = topEnglish;
        self.bottomEnglish = bottomEnglish;
        self.backHebrew = backHebrew
        self.topItalics = topItalics;
        self.middleItalics = middleItalics
        self.bottomItalics = bottomItalics

        super.init(type :type, numericalIndex: numericalIndex, alphabeticalIndex: alphabeticalIndex, frontHebrew: frontHebrew, chapterDescription: chapterDescription, redCoding: redCoding);

    }

    required init(coder aDecoder: NSCoder) {


        self.backHebrew = aDecoder.decodeObjectForKey("backHebrew") as! String;
        self.topEnglish = aDecoder.decodeObjectForKey("topEnglish") as! String;
        self.bottomEnglish = aDecoder.decodeObjectForKey("bottomEnglish") as! String;
        self.topItalics = aDecoder.decodeObjectForKey("topItalics") as! String;
        self.middleItalics = aDecoder.decodeObjectForKey("middleItalics") as! String;
        self.bottomItalics = aDecoder.decodeObjectForKey("bottomItalics") as! String;

        super.init(coder: aDecoder);
    }

    override func encodeWithCoder(aCoder: NSCoder) {

        aCoder.encodeObject(backHebrew, forKey: "backHebrew");
        aCoder.encodeObject(topEnglish, forKey: "topEnglish");
        aCoder.encodeObject(bottomEnglish, forKey: "bottomEnglish");
        aCoder.encodeObject(topItalics, forKey: "topItalics");
        aCoder.encodeObject(middleItalics, forKey: "middleItalics");
        aCoder.encodeObject(bottomItalics, forKey: "bottomItalics")

        super.encodeWithCoder(aCoder);
        
    }
    
}