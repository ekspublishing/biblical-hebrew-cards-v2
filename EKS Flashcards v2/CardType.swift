//
//  File.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/13/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

enum CardType:Int32 {
    case a
    case b
    case c
    case d
    case e
    case f1
    case f2
    case h
    case a1
    case b2
    case e1
    case r1
    case r2
    case r3
    case r4
    case r5
}
