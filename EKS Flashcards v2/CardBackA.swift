//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackA: CardFace {

    let nibName = "CardBackA"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var hebrewLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        let font = UIFont(name: Catalog.fontName, size: 30);
        hebrewLabel.font = font;

        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 12;
        englishLabel.font = englishLabel.font.withSize(fontSize);

        //Hebrew font is weirdly small
        hebrewLabel.font = hebrewLabel.font.withSize(fontSize * 2 );

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting();
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let aCard = backingCard as! CardA

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            var combo = aCard.backEnglish + " " + aCard.backItalics + " " as NSString;

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"
            combo = combo.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString

            let combinedString = NSMutableAttributedString(string: combo as String);
            if (aCard.backItalics != ""){
                let range = combo.range(of: aCard.backItalics, options: NSString.CompareOptions.backwards)
                let itFont = UIFont(name: "HoeflerText-Italic", size: englishLabel.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            englishLabel.attributedText = combinedString;

            #if PHCARDS
                changeCommaSize(aCard.backHebrew, targetMutableString: &hebrewLabel.attributedText!, size: hebrewLabel.font.pointSize / 1.5)
            #endif

        }

    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let aCard = backingCard as! CardA

            let hebrew = aCard.backHebrew as NSString

            let formattedHebrew = NSMutableAttributedString(string: hebrew as String)

            if (formattedHebrew.length != 0){
                let range = hebrew.range(of: "־")
                formattedHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(hebrewLabel.font.pointSize/14), range: range)
            }
            hebrewLabel.attributedText = formattedHebrew

        }
    }

    override func setCard(_ card :Card){

        if let card_A = card as? CardA {
            chapterDescriptionLabel.text = card_A.chapterDescription;
            englishLabel.text = card_A.backEnglish;
            hebrewLabel.text = card_A.backHebrew;
            numericalIndexLabel.text = String(card_A.numericalIndex);
            backingCard = card;

            applyFormatting();
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.a

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
