//
//  ReviewOnlyModeTableViewCell.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class ReviewOnlyModeTableViewCell: UITableViewCell {

    @IBOutlet weak var reviewOnlySwitch: UISwitch!
    
    @IBAction func reviewModeToggled(_ sender: UISwitch) {
        let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
        
        //TODO prevent this from happeneing if there are no review cards in the deck
        if let reviewChapter = appDelegate.catalog?.revewChapter {
        
            if (!appDelegate.isReviewMode && reviewChapter.count() == 0){
            
                //Error
                let alertController = UIAlertController(title: "Review Set is Empty", message: "The Review Set is empty. Please add cards to the review set before enabling review mode.", preferredStyle: UIAlertController.Style.alert);
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
                self.window?.rootViewController?.present(alertController, animated: true, completion: nil);
                
                reviewOnlySwitch.setOn(false, animated: true);
                
            } else {
                appDelegate.isReviewMode = !appDelegate.isReviewMode;
                appDelegate.dirtyDeck = true;
            }
        }
    
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        syncSwitchState();
    }
    
    func syncSwitchState(){
        let appDelegate = UIApplication.shared.delegate as! EKSAppDelegate
        reviewOnlySwitch.setOn(appDelegate.isReviewMode, animated: false);
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //weird apple behvaior where setselected gets fired during table intialization and then the switch toggles without firing the handler above
        if(animated){
            reviewModeToggled(reviewOnlySwitch)
        }

        // Configure the view for the selected state
    }

}
