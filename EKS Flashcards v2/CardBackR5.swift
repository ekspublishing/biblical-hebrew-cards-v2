
import UIKit

class CardBackR5: RedCardFace {

    let nibName = "CardBackR5"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var StackView: UIStackView!

    @IBOutlet weak var topEnglishLabel: UILabel!
    @IBOutlet weak var bottomEnglishLabel: UILabel!
    @IBOutlet weak var hebrewLabel: UILabel!
    @IBOutlet weak var italicsLabel: UILabel!


    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        let font = UIFont(name: Catalog.fontName, size: 30);
        hebrewLabel.font = font;

        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 12;
        topEnglishLabel.font = topEnglishLabel.font.withSize(fontSize);
        bottomEnglishLabel.font = bottomEnglishLabel.font.withSize(fontSize);

        //Hebrew font is weirdly small
        hebrewLabel.font = hebrewLabel.font.withSize(fontSize * 1.5);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 20
        italicsLabel.font = italicsLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        if (hideSmallViews){
            StackView.spacing = 3
        }
        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting()
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let r5Card = backingCard as! CardR5

            let hebrew = r5Card.backHebrew as NSString

            let formattedHebrew = NSMutableAttributedString(string: hebrew as String)
            if (formattedHebrew.length != 0){
                let range = hebrew.range(of: "־")
                formattedHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(hebrewLabel.font.pointSize/14), range: range)
            }
            hebrewLabel.attributedText = formattedHebrew

        }
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil){
            let r5Card = backingCard as! CardR5

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            var combo = r5Card.topEnglish + " " + r5Card.topItalics + " " as NSString;
            let combo2 = r5Card.bottomEnglish + " " + r5Card.middleItalics + " " as NSString;

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"

            combo = combo.stringByReplacingOccurrencesOfString("\\n", withString: myNewLineStr)
            combo2 = combo2.stringByReplacingOccurrencesOfString("\\n", withString: myNewLineStr)

            var combinedString = NSMutableAttributedString(string: combo as String);
            if (r5Card.topItalics.count != 0){
                let range = combo.range(of: r5Card.topItalics, options: NSString.CompareOptions.backwards)
                let itFont = UIFont(name: "HoeflerText-Italic", size: topEnglishLabel.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            topEnglishLabel.attributedText = combinedString;

            combinedString = NSMutableAttributedString(string: combo2 as String);
            if (r5Card.middleItalics.count != 0){
                let range = combo2.range(of: r5Card.middleItalics, options: NSString.CompareOptions.backwards)
                let itFont = UIFont(name: "HoeflerText-Italic", size: bottomEnglishLabel.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            bottomEnglishLabel.attributedText = combinedString;

            applyRed(r5Card.backHebrew, redIndexString: r5Card.redCoding, targetMutableString: &hebrewLabel.attributedText!)
        }
    }

    override func setCard(_ card :Card){

        if let card_R5 = card as? CardR5 {
            chapterDescriptionLabel.text = card_R5.chapterDescription;
            topEnglishLabel.text = card_R5.topEnglish;
            bottomEnglishLabel.text = card_R5.bottomEnglish;
            hebrewLabel.text = card_R5.backHebrew;
            italicsLabel.text = card_R5.bottomItalics
            numericalIndexLabel.text = String(card_R5.numericalIndex);
            backingCard = card;

            applyFormatting()
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.r5

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
