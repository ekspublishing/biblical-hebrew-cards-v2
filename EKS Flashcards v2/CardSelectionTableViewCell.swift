//
//  CardSelectionTableViewCell.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/6/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//


import UIKit

class CardSelectionTableViewCell: UITableViewCell {
    
    var backingCard :Card?;
    
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardEnabledSwitch: UISwitch!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var inReviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func enabledValueChanged(_ sender: AnyObject) {
        if let c = backingCard {
            c.isEnabled = !c.isEnabled;
            setDisabled(!c.isEnabled, animated: true);
        }

    }
    
    func setCard(_ card :Card){
        self.backingCard = card;
        self.cardView.setCard(card);
        self.cardView.setBorder(true);
        self.cardLabel.text = "Card \(card.numericalIndex)";
        self.cardEnabledSwitch.setOn(card.isEnabled, animated: false);
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setDisabled(_ isDisabled :Bool, animated :Bool){

        if(isDisabled){

            if (animated) {
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.cardView.alpha = 0.3; self.cardLabel.alpha = 0.3; self.inReviewLabel.alpha = 0.8
                    }, completion: nil);
            } else {
                self.cardView.alpha = 0.3
                self.cardLabel.alpha = 0.3
                self.inReviewLabel.alpha = 0.8
            }

        } else {
            if (animated) {
                UIView.animate(withDuration: 0.5, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    self.cardView.alpha = 1.0; self.cardLabel.alpha = 1.0; self.inReviewLabel.alpha = 1.0
                    }, completion: nil);
            } else {
                self.cardView.alpha = 1.0
                self.cardLabel.alpha = 1.0
                self.inReviewLabel.alpha = 1.0
            }
        }
    }
}
