//
//  Card.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/5/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import Foundation

open class Card: NSObject, NSCoding {

    var numericalIndex :Int;
    var alphabeticalIndex :Int;
    var frontHebrew :String
    var chapterDescription :String;
    var audioURL :URL?;
    var type :CardType;
    var redCoding :String?


    //TODO Add Audio File Pointer


    //State
    var isFlipped = false;
    var isEnabled = true;

    open override var hash: Int {
        return numericalIndex;
    }

    init(type :CardType, numericalIndex :Int, alphabeticalIndex :Int, frontHebrew :String, chapterDescription :String, redCoding :String?){

        self.type = type;
        self.numericalIndex = numericalIndex;
        self.alphabeticalIndex = alphabeticalIndex;
        self.frontHebrew = frontHebrew;
        self.chapterDescription = chapterDescription;
        self.redCoding = redCoding;

        #if PHCARDS
            let fileName = String(format: "s-%i", numericalIndex);
            let audioPath = Bundle.main.path(forResource: fileName, ofType: "mp3", inDirectory: "PH_Sounds");
        #else
            let fileName = String(format: "s%03i", numericalIndex);
            let audioPath = Bundle.main.path(forResource: fileName, ofType: "mp3", inDirectory: "BH_Sounds");
        #endif
        if (audioPath != nil){
            self.audioURL = URL(fileURLWithPath: audioPath!);
        }
        
    }

    required public init(coder aDecoder: NSCoder) {

        self.type = CardType(rawValue: aDecoder.decodeInt32(forKey: "type"))!;

        self.numericalIndex = aDecoder.decodeInteger(forKey: "numericalIndex");
        self.alphabeticalIndex = aDecoder.decodeInteger(forKey: "alphabeticalIndex");

        self.frontHebrew = aDecoder.decodeObject(forKey: "frontHebrew") as! String;
        self.chapterDescription = aDecoder.decodeObject(forKey: "chapterDescription") as! String;

        self.redCoding = (aDecoder.decodeObject(forKey: "redCoding") as? String);

        #if PHCARDS
            let fileName = String(format: "s-%i", numericalIndex);
            let audioPath = Bundle.main.path(forResource: fileName, ofType: "mp3",inDirectory: "PH_Sounds");
        #else
            let fileName = String(format: "s%03i", numericalIndex);
            let audioPath = Bundle.main.path(forResource: fileName, ofType: "mp3",inDirectory: "BH_Sounds");
        #endif

        self.audioURL = URL(fileURLWithPath: audioPath!);
    }

    open func encode(with aCoder: NSCoder) {

        aCoder.encodeCInt(type.rawValue, forKey: "type")

        aCoder.encode(numericalIndex, forKey: "numericalIndex");
        aCoder.encode(alphabeticalIndex, forKey: "alphabeticalIndex");

        aCoder.encode(frontHebrew, forKey: "frontHebrew");
        aCoder.encode(chapterDescription, forKey: "chapterDescription");

        aCoder.encode(redCoding, forKey: "redCoding")
    }

}

public func == (lhs: Card, rhs: Card) -> Bool {
    return (lhs.numericalIndex == rhs.numericalIndex);
}
