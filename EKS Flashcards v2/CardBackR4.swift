
import UIKit

class CardBackR4: RedCardFace {

    let nibName = "CardBackR4"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var topHebrewLabel: UILabel!
    @IBOutlet weak var bottomHebrewLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();

        let font = UIFont(name: Catalog.fontName, size: 30);
        topHebrewLabel.font = font;
        bottomHebrewLabel.font = font

        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 12;
        englishLabel.font = englishLabel.font.withSize(fontSize);

        //Hebrew font is weirdly small
        topHebrewLabel.font = topHebrewLabel.font.withSize(fontSize * 1.5 );
        bottomHebrewLabel.font = bottomHebrewLabel.font.withSize(fontSize * 1.5);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting()
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let r4Card = backingCard as! CardR4

            let hebrew1 = r4Card.topHebrew as NSString
            let hebrew2 = r4Card.bottomHebrew as NSString

            let formattedHebrew1 = NSMutableAttributedString(string: hebrew1 as String)
            if (formattedHebrew1.length != 0){
                let range = hebrew1.range(of: "־")
                formattedHebrew1.addAttribute(NSAttributedString.Key.baselineOffset, value: -(topHebrewLabel.font.pointSize/14), range: range)
            }
            topHebrewLabel.attributedText = formattedHebrew1

            let formattedHebrew2 = NSMutableAttributedString(string: hebrew2 as String)
            if (formattedHebrew2.length != 0){
                let range = hebrew2.range(of: "־")
                formattedHebrew2.addAttribute(NSAttributedString.Key.baselineOffset, value: -(bottomHebrewLabel.font.pointSize/14), range: range)
            }
            bottomHebrewLabel.attributedText = formattedHebrew2

        }
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil){
            let r4Card = backingCard as! CardR4

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            //the trailing space is to handle a clipping issue.
            var combo1 = r4Card.backEnglish + " " + r4Card.topItalics + " " as NSString;
            var combo2 = r4Card.bottomHebrew + " " + r4Card.bottomItalics + " " as NSString;
            combo2 = "\u{202A}\(combo2)\u{202C}" as NSString

            //insert line break in string when there is \n
            let myNewLineStr: String = "\n"
            combo1 = combo1.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString
            combo2 = combo2.replacingOccurrences(of: "\\n", with: myNewLineStr) as NSString

            var combinedString = NSMutableAttributedString(string: combo1 as String);
            if (r4Card.topItalics.count != 0){
                let range = combo1.range(of: r4Card.topItalics, options: NSString.CompareOptions.backwards)
                let itFont = UIFont(name: "HoeflerText-Italic", size: englishLabel.font.pointSize/2)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }

            englishLabel.attributedText = combinedString;

            combinedString = NSMutableAttributedString(string: combo2 as String)
            if (r4Card.bottomItalics.count != 0){
                let range = combo2.range(of: r4Card.bottomItalics)
                let itFont = UIFont(name: "HoeflerText-Italic", size: bottomHebrewLabel.font.pointSize/2.5)
                combinedString.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            bottomHebrewLabel.attributedText = combinedString

            applyRed(r4Card.topHebrew, redIndexString: r4Card.redCoding, targetMutableString: &topHebrewLabel.attributedText!)
        }
    }

    override func setCard(_ card :Card){

        if let card_R4 = card as? CardR4 {
            chapterDescriptionLabel.text = card_R4.chapterDescription;
            englishLabel.text = card_R4.backEnglish;
            topHebrewLabel.text = card_R4.topHebrew;
            bottomHebrewLabel.text = card_R4.bottomHebrew;
            numericalIndexLabel.text = String(card_R4.numericalIndex);
            backingCard = card;

            applyFormatting()
        }
    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.r4

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
