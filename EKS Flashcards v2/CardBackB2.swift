//
//  CardView.swift
//  EKS Flashcards v2
//
//  Created by Andrew Budker on 12/8/15.
//  Copyright © 2015 EKS Publishing. All rights reserved.
//

import UIKit

class CardBackB2: CardFace {

    let nibName = "CardBackB2"
    var backingCard :Card?

    @IBOutlet weak var chapterDescriptionLabel: UILabel!
    @IBOutlet weak var numericalIndexLabel: UILabel!

    @IBOutlet weak var topEnglishLabel: UILabel!
    @IBOutlet weak var leftHebrewLabel: UILabel!
    @IBOutlet weak var rightHebrewLabel: UILabel!

    // Our custom view from the XIB file
    var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame);
        xibSetup();
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        xibSetup();
    }

    override func layoutSubviews() {

        super.layoutSubviews();


        leftHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);
        rightHebrewLabel.font = UIFont(name: Catalog.fontName, size: 30);


        //Adjust font sizes
        let rootView = self.view;
        var fontSize = CGFloat((rootView?.frame.size.width)!) / 16;

        leftHebrewLabel.font = leftHebrewLabel.font.withSize(fontSize * 2);
        topEnglishLabel.font = topEnglishLabel.font.withSize(fontSize);
        rightHebrewLabel.font = rightHebrewLabel.font.withSize(fontSize * 2);

        fontSize = CGFloat((rootView?.frame.size.width)!) / 26;
        numericalIndexLabel.font = numericalIndexLabel.font.withSize(fontSize);
        chapterDescriptionLabel.font = chapterDescriptionLabel.font.withSize(fontSize);

        //hide extra information if the cards are small
        let hideSmallViews = (rootView?.frame.size.width)! < CGFloat(300);

        numericalIndexLabel.isHidden = hideSmallViews;
        chapterDescriptionLabel.isHidden = hideSmallViews;

        applyFormatting()
    }

    fileprivate func hebrewMaqafFormatting(){
        if (backingCard != nil){
            let bCard = backingCard as! CardB2

            let lHebrew = bCard.leftHebrew as NSString
            let rHebrew = bCard.rightHebrew as NSString

            let formattedLeftHebrew = NSMutableAttributedString(string: lHebrew as String)
            let formattedRightHebrew = NSMutableAttributedString(string: rHebrew as String)

            if (formattedLeftHebrew.length != 0){
                let range = lHebrew.range(of: "־")
                formattedLeftHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(leftHebrewLabel.font.pointSize/14), range: range)
            }
            leftHebrewLabel.attributedText = formattedLeftHebrew

            if (formattedRightHebrew.length != 0){
                let range = rHebrew.range(of: "־")
                formattedRightHebrew.addAttribute(NSAttributedString.Key.baselineOffset, value: -(rightHebrewLabel.font.pointSize/14), range: range)
            }
            rightHebrewLabel.attributedText = formattedRightHebrew
        }
    }

    fileprivate func applyFormatting(){

        if (backingCard != nil) {
            let b2Card = backingCard as! CardB2

            //format the maqaf in the hebrew if there is one
            //commented out for PH font
            //hebrewMaqafFormatting()

            let lCombo = b2Card.leftItalics + " " + b2Card.leftHebrew + " " as NSString
            let rCombo = b2Card.rightItalics + " " + b2Card.rightHebrew + " " as NSString

            let formattedLeftCombo = NSMutableAttributedString(string: lCombo as String)
            let formattedRightCombo = NSMutableAttributedString(string: rCombo as String)

            if (formattedLeftCombo.length != 0){
                let range = lCombo.range(of: b2Card.leftItalics)
                let itFont = UIFont(name: "HoeflerText-Italic", size: leftHebrewLabel.font.pointSize/2)
                formattedLeftCombo.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            leftHebrewLabel.attributedText = formattedLeftCombo

            if (formattedRightCombo.length != 0){
                let range = rCombo.range(of: b2Card.rightItalics)
                let itFont = UIFont(name: "HoeflerText-Italic", size: rightHebrewLabel.font.pointSize/2)
                formattedRightCombo.addAttribute(NSAttributedString.Key.font, value: itFont!, range: range)
            }
            rightHebrewLabel.attributedText = formattedRightCombo
        }
    }

    override func setCard(_ card :Card){

        if let card_B2 = card as? CardB2 {
            chapterDescriptionLabel.text = card_B2.chapterDescription;

            topEnglishLabel.text = card_B2.topEnglish

            leftHebrewLabel.text = card_B2.leftHebrew;

            rightHebrewLabel.text = card_B2.rightHebrew;

            numericalIndexLabel.text = String(card_B2.numericalIndex);

            backingCard = card;

            applyFormatting()
        }

    }

    func xibSetup() {

        //Set the card type
        cardType = CardType.b2

        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
